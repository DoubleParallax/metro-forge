package com.doubleparallax.metro.core.transformer;

import com.doubleparallax.metro.core.transformer.descriptor.ClassDescriptor;
import com.doubleparallax.metro.core.transformer.descriptor.DescribedMethod;
import cpw.mods.modlauncher.api.ITransformer;
import org.objectweb.asm.tree.ClassNode;

@ClassDescriptor("net.minecraftforge.fml.client.ClientModLoader")
public class ClientModLoaderTransformer extends ClassTransformer implements ITransformer<ClassNode> {

    @Override
    protected void init() {
        methods.add(new DescribedMethod("completeModLoading", "()Z", DescribedMethod.InsertType.BEFORE, () -> {
            return "com.doubleparallax.metro.Metro.onPostInitialize();";
        }));
    }

}
