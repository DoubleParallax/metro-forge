package com.doubleparallax.metro.core.transformer;

import com.doubleparallax.metro.core.transformer.descriptor.ClassDescriptor;
import com.doubleparallax.metro.core.transformer.descriptor.DescribedMethod;
import cpw.mods.modlauncher.api.ITransformer;
import cpw.mods.modlauncher.api.ITransformerVotingContext;
import cpw.mods.modlauncher.api.TransformerVoteResult;
import javassist.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public abstract class ClassTransformer implements ITransformer<ClassNode> {

    private static final Logger LOGGER = LogManager.getLogger();

    private final Set<Target> targets = new HashSet<>();
    protected final Set<DescribedMethod> methods = new HashSet<>();

    protected ClassTransformer() {
        ClassDescriptor descriptor = getClass().getAnnotation(ClassDescriptor.class);
        if (descriptor == null)
            throw new IllegalStateException("Class descriptor must be provided for class transformer " + getClass().getSimpleName());
        for (String cls : descriptor.value())
            targets.add(ITransformer.Target.targetClass(cls));
        init();
    }

    protected abstract void init();

    @Nonnull
    @Override
    public ClassNode transform(ClassNode input, ITransformerVotingContext context) {
        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        input.accept(writer);
        byte[] byteArray = writer.toByteArray();

        ClassPool classPool = ClassPool.getDefault();
        try {
            CtClass ctClass = classPool.makeClass(new ByteArrayInputStream(byteArray));
            for (DescribedMethod method : methods) {
                try {
                    CtBehavior behavior = method.getMethod().equalsIgnoreCase("<init>")
                            ? ctClass.getConstructor(method.getSignature())
                            : ctClass.getMethod(method.getMethod(), method.getSignature());
                    switch (method.getInsertType()) {
                        case BEFORE:
                            behavior.insertBefore(method.getCode());
                            break;
                        case AFTER:
                            behavior.insertAfter(method.getCode());
                            break;
                        case AT:
                            behavior.insertAt(method.getInsertAt(), method.getCode());
                            break;
                    }
                } catch (NotFoundException e) {
                    LOGGER.warn("Failed to get method " + method.getMethod() + " for class " + ctClass.getSimpleName(), e);
                } catch (CannotCompileException e) {
                    LOGGER.warn(String.format("Failed to insert code %s line %s of method %s for class %s", method.getInsertType(), method.getInsertAt(), method.getMethod(), ctClass.getSimpleName()), e);
                }
            }
            byteArray = ctClass.toBytecode();
        } catch (IOException e) {
            LOGGER.warn("Javassist failed to make class from byte array, aborting transformation", e);
        } catch (CannotCompileException e) {
            LOGGER.warn("Javassist failed to compile class from bytecode, aborting transformation", e);
        }

        ClassNode node = new ClassNode();
        ClassReader reader = new ClassReader(byteArray);
        reader.accept(node, 0);
        return node;
    }

    @Nonnull
    @Override
    public TransformerVoteResult castVote(ITransformerVotingContext context) {
        return TransformerVoteResult.YES;
    }

    @Nonnull
    @Override
    public Set<Target> targets() {
        return targets;
    }
}
