package com.doubleparallax.metro.core.transformer;

import com.doubleparallax.metro.core.transformer.descriptor.ClassDescriptor;
import com.doubleparallax.metro.core.transformer.descriptor.DescribedMethod;
import cpw.mods.modlauncher.api.ITransformer;
import org.objectweb.asm.tree.ClassNode;

@ClassDescriptor("net.minecraft.util.MinecraftVersion")
public class MinecraftVersionTransformer extends ClassTransformer implements ITransformer<ClassNode> {

    @Override
    protected void init() {
        methods.add(new DescribedMethod("getProtocolVersion", "()I", DescribedMethod.InsertType.BEFORE, () -> {
            return "com.doubleparallax.metro.event.impl.GetMinecraftProtocolEvent getMinecraftProtocolEvent = new com.doubleparallax.metro.event.impl.GetMinecraftProtocolEvent().setProtocol(this.protocolVersion);" +
                    "com.doubleparallax.metro.event.EventFactory eventFactory = (com.doubleparallax.metro.event.EventFactory) com.doubleparallax.metro.component.ComponentFactory.INSTANCE.getComponent(com.doubleparallax.metro.event.EventFactory.class);" +
                    "eventFactory.onEvent(getMinecraftProtocolEvent);" +
                    "if (getMinecraftProtocolEvent.isCancelled())" +
                    "   return getMinecraftProtocolEvent.getProtocol();";
        }));
    }

}
