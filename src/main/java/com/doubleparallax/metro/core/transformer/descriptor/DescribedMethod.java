package com.doubleparallax.metro.core.transformer.descriptor;

import java.util.function.Supplier;

public class DescribedMethod {

    public enum InsertType {
        BEFORE,
        AFTER,
        AT
    }

    private final String method, signature, code;
    private final InsertType insertType;
    private final int insertAt;

    public DescribedMethod(String method, String signature, InsertType insertType, Supplier<String> code) {
        this(method, signature, insertType, 0, code);
    }

    public DescribedMethod(String method, String signature, InsertType insertType, int insertAt, Supplier<String> code) {
        this.method = method;
        this.signature = signature;
        this.insertType = insertType;
        this.insertAt = insertAt;
        this.code = code.get();
    }

    public String getMethod() {
        return method;
    }

    public String getSignature() {
        return signature;
    }

    public String getCode() {
        return code;
    }

    public InsertType getInsertType() {
        return insertType;
    }

    public int getInsertAt() {
        return insertAt;
    }
}
