package com.doubleparallax.metro.core;

import com.doubleparallax.metro.core.transformer.ClassTransformer;
import cpw.mods.modlauncher.api.IEnvironment;
import cpw.mods.modlauncher.api.ITransformationService;
import cpw.mods.modlauncher.api.ITransformer;
import net.minecraftforge.coremod.CoreModEngine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

public class TransformationFactory implements ITransformationService {

    private static final Logger LOGGER = LogManager.getLogger();

    private CoreModEngine engine = new CoreModEngine();

    @Nonnull
    @Override
    public String name() {
        return "metroCore";
    }

    @Override
    public void initialize(IEnvironment environment) {
    }

    @Override
    public void beginScanning(IEnvironment environment) {
    }

    @Override
    public void onLoad(IEnvironment env, Set<String> otherServices) {
    }

    @Nonnull
    @Override
    public List<ITransformer> transformers() {
        LOGGER.debug("Initializing transformers");
        List<ITransformer<?>> transformers = engine.initializeCoreMods();

        LOGGER.debug("Getting custom transformers");
        new Reflections()
                .getSubTypesOf(ClassTransformer.class)
                .forEach((type) -> {
                    try {
                        LOGGER.debug("Initializing transformer " + type.getSimpleName());
                        transformers.add(type.getConstructor().newInstance());
                        LOGGER.debug("Initialized transformer " + type.getSimpleName());
                    } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
                        LOGGER.warn("Failed to initialize transformer " + type.getSimpleName(), e);
                    }
                });

        return (List) transformers;
    }
}
