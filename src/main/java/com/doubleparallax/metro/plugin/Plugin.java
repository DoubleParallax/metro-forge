package com.doubleparallax.metro.plugin;

import com.doubleparallax.metro.attribute.Attribute;
import com.doubleparallax.metro.attribute.AttributeDescriptor;
import com.doubleparallax.metro.command.Command;
import com.doubleparallax.metro.command.CommandFactory;
import com.doubleparallax.metro.command.impl.PluginCommand;
import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.util.MinecraftHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;

public abstract class Plugin extends Attribute implements MinecraftHelper {

    private static final Logger LOGGER = LogManager.getLogger();

    @AttributeDescriptor("category")
    public PluginCategory pluginCategory = PluginCategory.MISC;

    @AttributeDescriptor("active")
    public boolean active;

    public Plugin() {
        CommandFactory commandFactory = ComponentFactory.INSTANCE.getComponent(CommandFactory.class);
        Command command = new PluginCommand(this);
        commandFactory.put(command.getAlias(), command);
    }

    @Override
    protected void onFieldValueChange(Field field, Object value) {
        LOGGER.debug("Field " + field.getName() + " value changed to " + value);
    }

}
