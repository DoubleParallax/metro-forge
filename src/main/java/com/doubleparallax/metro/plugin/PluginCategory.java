package com.doubleparallax.metro.plugin;

import com.doubleparallax.metro.util.Locale;

public enum PluginCategory {

    AUTO,
    COMBAT,
    MOVEMENT,
    RENDER,
    WORLD,
    MISC;

    private final String displayName;

    PluginCategory() {
        this.displayName = Locale.translate(String.format("plugin.category.%s.displayName", name().toLowerCase()));
    }

    public String getDisplayName() {
        return displayName;
    }

}
