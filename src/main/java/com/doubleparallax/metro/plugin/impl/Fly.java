package com.doubleparallax.metro.plugin.impl;

import com.doubleparallax.metro.attribute.AttributeDescriptor;
import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.event.EventFactory;
import com.doubleparallax.metro.event.impl.LivingEntityUpdateEvent;
import com.doubleparallax.metro.plugin.Plugin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;

@AttributeDescriptor("fly")
public class Fly extends Plugin {

    private static final Logger LOGGER = LogManager.getLogger();

    //@formatter:off

    @AttributeDescriptor("speed")
    public int speed = 5;

    @AttributeDescriptor(parent = "speed", value = "min")
    public int speedMin = 1;

    @AttributeDescriptor(parent = "speed", value = "max")
    public int speedMax = 10;

    //@formatter:on

    private float previousFlySpeed;

    public Fly() {
        EventFactory eventFactory = ComponentFactory.INSTANCE.getComponent(EventFactory.class);
        eventFactory.register(LivingEntityUpdateEvent.class, this::onEvent);
    }

    @Override
    protected void onFieldValueChange(Field field, Object value) {
        if (field.getName().equalsIgnoreCase("active")) {
            if (mc.player == null) {
                LOGGER.warn("Minecraft player is null when changing plugin " + getId() + " active state");
                return;
            }
            if (active) {
                previousFlySpeed = mc.player.abilities.getFlySpeed();
                LOGGER.debug("Enabling plugin " + getId() + ", storing current player fly speed");
            } else {
                mc.player.abilities.setFlySpeed(previousFlySpeed);
                LOGGER.debug("Disabling plugin " + getId() + ", resetting player fly speed");
            }
        }
    }

    private void onEvent(LivingEntityUpdateEvent e) {
        if (!active) return;
        if (e.getEntity() != mc.player) return;
        if (mc.player == null) {
            LOGGER.warn("Minecraft player is null when receiving " + LivingEntityUpdateEvent.class.getSimpleName());
            return;
        }
        if (speed < speedMin) {
            LOGGER.debug(String.format("Speed (%s) below minimum (%s), capping", speed, speedMin));
            speed = speedMin;
        }
        if (speed > speedMax) {
            LOGGER.debug(String.format("Speed (%s) above maximum (%s), capping", speed, speedMax));
            speed = speedMax;
        }
        mc.player.abilities.isFlying = true;
        mc.player.abilities.setFlySpeed(speed);
    }

}
