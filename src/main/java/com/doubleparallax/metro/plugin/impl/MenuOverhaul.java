package com.doubleparallax.metro.plugin.impl;

import com.doubleparallax.metro.attribute.AttributeDescriptor;
import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventFactory;
import com.doubleparallax.metro.event.impl.ClientUpdateEvent;
import com.doubleparallax.metro.event.impl.KeyboardEvent;
import com.doubleparallax.metro.event.impl.MouseEvent;
import com.doubleparallax.metro.event.impl.ScreenEvent;
import com.doubleparallax.metro.plugin.Plugin;
import com.doubleparallax.metro.ui.UIFactory;
import com.doubleparallax.metro.ui.elements.interaction.InteractionException;
import com.doubleparallax.metro.ui.impl.UI;
import com.doubleparallax.metro.ui.screen.ModMenuScreen;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.Screen;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@AttributeDescriptor("menu-overhaul")
public class MenuOverhaul extends Plugin {

    private static final Logger LOGGER = LogManager.getLogger();

    //@formatter:off

    //@formatter:on

    private UI<?> currentUI;

    public MenuOverhaul() {
        super();

        EventFactory eventFactory = ComponentFactory.INSTANCE.getComponent(EventFactory.class);
        eventFactory.register(ScreenEvent.Open.class, this::onEvent);
        eventFactory.register(ClientUpdateEvent.class, this::onEvent);
        eventFactory.register(ScreenEvent.Render.class, this::onEvent);
        eventFactory.register(MouseEvent.Action.Pressed.class, this::onEvent);
        eventFactory.register(MouseEvent.Action.Released.class, this::onEvent);
        eventFactory.register(MouseEvent.Scrolled.class, this::onEvent);
        eventFactory.register(KeyboardEvent.Action.Pressed.class, this::onEvent);
        eventFactory.register(KeyboardEvent.Action.Released.class, this::onEvent);
        eventFactory.register(KeyboardEvent.CharTyped.class, this::onEvent);
    }

    private void onEvent(ScreenEvent.Open event) {
        if (event.getNewScreen() == null) {
            if (event.getOldScreen() instanceof ModMenuScreen && currentUI != null) {
                currentUI = null;
            }
            return;
        }

        Class<? extends Screen> screenClass = event.getNewScreen().getClass();
        UIFactory uiFactory = ComponentFactory.INSTANCE.getComponent(UIFactory.class);
        UI<?> ui = uiFactory.get(screenClass);
        if (ui == null) {
            LOGGER.debug("No UI found for screen " + screenClass.getSimpleName());
            if (currentUI != null) {
                currentUI = null;
            }
            return;
        }

        currentUI = ui;
        currentUI.init();
    }

    private void onEvent(ClientUpdateEvent event) {
        if (!active || currentUI == null)
            return;

        if (mc.getMainWindow().getWidth() != currentUI.getW(false) || mc.getMainWindow().getHeight() != currentUI.getH(false))
            currentUI.init();

        currentUI.update();
    }

    private void onEvent(ScreenEvent.Render event) {
        if (!active || currentUI == null)
            return;

        MatrixStack matrixStack = event.getMatrixStack();

        matrixStack.push();
        double guiScale = mc.getMainWindow().getGuiScaleFactor();
        float scale = (float) (guiScale / Math.pow(guiScale, 2));
        matrixStack.scale(scale, scale, scale);
        currentUI.render(event.getMatrixStack());
        matrixStack.pop();

        event.setCancelled(true);
    }

    private void onEvent(MouseEvent.Action.Pressed event) {
        if (!active || event.getState() == Event.State.POST || currentUI == null)
            return;
        try {
            currentUI.mouseClicked(event.getMx(), event.getMy(), event.getButton());
        } catch (InteractionException e) {
        }
        event.setCancelled(true);
    }

    private void onEvent(MouseEvent.Action.Released event) {
        if (!active || event.getState() == Event.State.POST || currentUI == null)
            return;
        try {
            currentUI.mouseReleased(event.getMx(), event.getMy(), event.getButton());
        } catch (InteractionException e) {
        }
        event.setCancelled(true);
    }

    private void onEvent(MouseEvent.Scrolled event) {
        if (!active || event.getState() == Event.State.POST || currentUI == null)
            return;
        try {
            currentUI.mouseScrolled(event.getMx(), event.getMy(), event.getScroll());
        } catch (InteractionException e) {
        }
        event.setCancelled(true);
    }

    private void onEvent(KeyboardEvent.Action.Pressed event) {
        if (!active || event.getState() == Event.State.POST || currentUI == null)
            return;
        try {
            currentUI.keyPressed(event.getKeyCode(), event.getScanCode(), event.getModifiers());
        } catch (InteractionException e) {
        }
        event.setCancelled(true);
    }

    private void onEvent(KeyboardEvent.Action.Released event) {
        if (!active || event.getState() == Event.State.POST || currentUI == null)
            return;
        try {
            currentUI.keyReleased(event.getKeyCode(), event.getScanCode(), event.getModifiers());
        } catch (InteractionException e) {
        }
        event.setCancelled(true);
    }

    private void onEvent(KeyboardEvent.CharTyped event) {
        if (!active || event.getState() == Event.State.POST || currentUI == null)
            return;
        try {
            currentUI.charTyped(event.getCharTyped(), event.getModifiers());
        } catch (InteractionException e) {
        }
        event.setCancelled(true);
    }

}
