package com.doubleparallax.metro.plugin.impl;

import com.doubleparallax.metro.attribute.AttributeDescriptor;
import com.doubleparallax.metro.command.CommandFactory;
import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.event.EventFactory;
import com.doubleparallax.metro.event.impl.ChatEvent;
import com.doubleparallax.metro.plugin.Plugin;
import com.doubleparallax.metro.util.MinecraftHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@AttributeDescriptor("commands")
public class Commands extends Plugin implements MinecraftHelper {

    private static final Logger LOGGER = LogManager.getLogger();
    private final CommandFactory commandFactory;

    //@formatter:off

    @AttributeDescriptor("cmd-prefix")
    public String cmdPrefix = ".";

    @AttributeDescriptor("arg-prefix")
    public String argPrefix = "--";

    //@formatter:on

    public Commands() {
        commandFactory = ComponentFactory.INSTANCE.getComponent(CommandFactory.class);

        EventFactory eventFactory = ComponentFactory.INSTANCE.getComponent(EventFactory.class);
        eventFactory.register(ChatEvent.class, this::onEvent);
    }

    private void onEvent(ChatEvent event) {
        LOGGER.debug("Client chat event received: " + event.getMessage());
        String message = event.getMessage();
        if (!message.startsWith(cmdPrefix)) {
            LOGGER.debug("No command prefix, ignoring");
            return;
        }

        if (mc.ingameGUI != null)
            mc.ingameGUI.getChatGUI().addToSentMessages(message);

        message = message.substring(cmdPrefix.length());
        String[] args = message.split(" ");

        if (commandFactory.execute(args)) event.setCancelled(true);
        else LOGGER.warn("Failed to execute command");
    }
}
