package com.doubleparallax.metro.plugin.impl;

import com.doubleparallax.metro.attribute.AttributeDescriptor;
import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.event.EventFactory;
import com.doubleparallax.metro.event.impl.ScreenEvent;
import com.doubleparallax.metro.plugin.Plugin;
import com.doubleparallax.metro.ui.screen.ModMenuScreen;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;

@AttributeDescriptor("mod-menu")
public class ModMenu extends Plugin {

    private static final Logger LOGGER = LogManager.getLogger();

    //@formatter:off

    //@formatter:on

    private boolean openOnClose;

    public ModMenu() {
        super();

        EventFactory eventFactory = ComponentFactory.INSTANCE.getComponent(EventFactory.class);
        eventFactory.register(ScreenEvent.Open.class, this::onEvent);
    }

    @Override
    protected void onFieldValueChange(Field field, Object value) {
        if (field.getName().equalsIgnoreCase("active")) {
            if (active) {
                openOnClose = true;
                active = false;
            }
        }
    }

    private void onEvent(ScreenEvent.Open event) {
        LOGGER.debug("Received " + event.getClass().getSimpleName() + " event, old screen " + event.getOldScreen() + ", new screen " + event.getNewScreen());
        if (event.getNewScreen() != null) {
            LOGGER.debug("New screen not null, ignoring");
            return;
        }
        if (openOnClose) {
            LOGGER.debug("Screen is null, opening " + ModMenuScreen.class);
            event.setCancelled(true);
            openOnClose = false;
            mc.displayGuiScreen(new ModMenuScreen());
        }
    }

}
