package com.doubleparallax.metro.plugin;

import com.doubleparallax.metro.Factory;
import com.doubleparallax.metro.component.Component;
import com.doubleparallax.metro.plugin.impl.MenuOverhaul;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;

@Component
public class PluginFactory extends Factory<Class<? extends Plugin>, Plugin> {

    private static final Logger LOGGER = LogManager.getLogger();

    public PluginFactory() {
        LOGGER.debug("Populating plugin factory map with subtypes of " + Plugin.class.getSimpleName());
        new Reflections()
                .getSubTypesOf(Plugin.class)
                .forEach((type) -> {
                    try {
                        LOGGER.debug("Initializing plugin class " + type.getSimpleName());
                        put(type, type.getConstructor().newInstance());
                        LOGGER.debug("Initialized plugin class " + type.getSimpleName());
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        LOGGER.error("There was an issue creating plugin " + type.getSimpleName(), e);
                    }
                });
        get(MenuOverhaul.class).active = true;
    }

}
