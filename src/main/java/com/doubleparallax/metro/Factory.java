package com.doubleparallax.metro;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class Factory<K, V> {

    private final Map<K, V> map = new HashMap<>();

    public void put(K key, V value) {
        map.put(key, value);
    }

    public V get(K key) {
        return map.get(key);
    }

    public Map<K, V> getMap() {
        return map;
    }

    public Set<K> getKeys() {
        return map.keySet();
    }

    public Collection<V> getValues() {
        return map.values();
    }

}
