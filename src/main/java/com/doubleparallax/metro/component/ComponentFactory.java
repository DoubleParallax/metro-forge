package com.doubleparallax.metro.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public enum ComponentFactory {
    INSTANCE;

    private static final Logger LOGGER = LogManager.getLogger();

    private final Map<Class<?>, Object> componentMap = new HashMap<>();

    public void initialize() {
        componentMap.clear();
        LOGGER.debug("Initializing components");
        List<Class<?>> componentClasses = new ArrayList<>(new Reflections().getTypesAnnotatedWith(Component.class));
        componentClasses.sort(Comparator.comparingInt(a -> a.getAnnotation(Component.class).priority()));
        componentClasses.forEach((type) -> {
            try {
                LOGGER.debug("Initializing component " + type.getSimpleName());
                componentMap.put(type, type.getConstructor().newInstance());
                LOGGER.debug("Initialized component " + type.getSimpleName());
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                LOGGER.error("There was an issue creating component " + type.getSimpleName(), e);
            }
        });
    }

    public <T> T getComponent(Class<T> cls) {
        Object obj = componentMap.get(cls);
        if (obj == null)
            throw new RuntimeException("Failed to find component " + cls.getSimpleName());
        if (!cls.isInstance(obj))
            throw new ClassCastException("Cannot cast component " + obj.getClass().getSimpleName() + " to " + cls.getSimpleName());
        return cls.cast(obj);
    }
}
