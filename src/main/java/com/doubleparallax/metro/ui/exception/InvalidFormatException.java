package com.doubleparallax.metro.ui.exception;

public class InvalidFormatException extends RuntimeException {

    public InvalidFormatException(String msg) {
        super(msg);
    }

    public InvalidFormatException(String msg, Throwable e) {
        super(msg, e);
    }

}
