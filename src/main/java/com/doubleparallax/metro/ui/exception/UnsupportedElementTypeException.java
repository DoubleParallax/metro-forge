package com.doubleparallax.metro.ui.exception;

public class UnsupportedElementTypeException extends RuntimeException {

    public UnsupportedElementTypeException(String msg) {
        super(msg);
    }

    public UnsupportedElementTypeException(String msg, Throwable e) {
        super(msg, e);
    }

}
