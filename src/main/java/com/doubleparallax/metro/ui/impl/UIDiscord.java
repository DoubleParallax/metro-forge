package com.doubleparallax.metro.ui.impl;

import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.ui.UIDescriptor;
import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.ui.elements.impl.*;
import com.doubleparallax.metro.ui.elements.interaction.InteractionException;
import com.doubleparallax.metro.ui.screen.DiscordScreen;
import com.doubleparallax.metro.util.render.font.FontFactory;
import com.doubleparallax.metro.util.render.font.impl.FontProfile;
import com.mojang.realmsclient.RealmsMainScreen;
import net.minecraft.client.MainWindow;
import net.minecraft.client.gui.screen.*;
import org.lwjgl.glfw.GLFW;

@UIDescriptor(DiscordScreen.class)
public class UIDiscord extends UI<UIDiscord> {

    public UIDiscord() {
        super();

        FontFactory fontFactory = ComponentFactory.INSTANCE.getComponent(FontFactory.class);
        FontProfile font = fontFactory.get("avenir:16.0");

        setOnKeyPressed((ui, action) -> {
            if (action.getKey() == GLFW.GLFW_KEY_ESCAPE)
                mc.displayGuiScreen(null);
        });

        add((ui) -> new Element<?>[]{
                new ResizeDragPanel().setOffset(80, 80).setSize(ui.getW(true) - 160, ui.getH(true) - 160).setPadding(10).setResizable(true).setDraggable(true).add((window) -> new Element<?>[]{
                        new Scrollable().setOffset(0, 0).setSize(64, window.getH(true)).setAnchor(true, true, false, true).add((scrollable) -> new Element<?>[]{
                                new ElementList().setOffset(0, 0).setSize(scrollable.getW(true), scrollable.getH(true)).setAnchor(true).add((elementList) -> new Element<?>[]{
                                        new IconButton().setSize(elementList.getW(true), elementList.getW(true)).setAnchor(true, true, true, false),
                                        new IconButton().setSize(elementList.getW(true), elementList.getW(true)).setAnchor(true, true, true, false)
                                })
                        }),
                        new Scrollable().setOffset(65, 0).setSize(window.getW(true), window.getH(true)).setAnchor(true).add((scrollable) -> new Element<?>[]{
                                new ElementList().setOffset(0, 0).setSize(scrollable.getW(true), scrollable.getH(true)).setAnchor(true).add((elementList) -> new Element<?>[]{
                                        new Button().setSize(elementList.getW(true), 32).setPadding(16, 0, 16, 0).setAnchor(true, true, true, false).add((button) -> new Element<?>[]{
                                                new Label().setOffset(0, 0).setSize(button.getW(true), button.getH(true)).setAnchor(true).setLabel("Singleplayer").setVerAlign(Label.Align.CENTER).setFontRenderer(font).setColor(-1)
                                        }).setOnMouseClicked((button, action) -> {
                                            if (button.isHovered() && action.getButton() == GLFW.GLFW_MOUSE_BUTTON_LEFT)
                                                throw new InteractionException();
                                        }).setOnMouseReleased((button, action) -> {
                                            if (button.isHovered() && action.getButton() == GLFW.GLFW_MOUSE_BUTTON_LEFT)
                                                mc.displayGuiScreen(null);
                                        }),
                                })
                        })
                })
        });
    }
}
