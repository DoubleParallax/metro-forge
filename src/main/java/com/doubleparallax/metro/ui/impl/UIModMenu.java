package com.doubleparallax.metro.ui.impl;

import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.plugin.Plugin;
import com.doubleparallax.metro.plugin.PluginCategory;
import com.doubleparallax.metro.plugin.PluginFactory;
import com.doubleparallax.metro.ui.UIDescriptor;
import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.ui.elements.impl.*;
import com.doubleparallax.metro.ui.screen.ModMenuScreen;
import com.doubleparallax.metro.util.render.font.FontFactory;
import com.doubleparallax.metro.util.render.font.impl.FontProfile;

import java.util.HashSet;
import java.util.Set;

@UIDescriptor(ModMenuScreen.class)
public class UIModMenu extends UI<UIModMenu> {

    public UIModMenu() {
        super();

        FontFactory fontFactory = ComponentFactory.INSTANCE.getComponent(FontFactory.class);
        PluginFactory pluginFactory = ComponentFactory.INSTANCE.getComponent(PluginFactory.class);

        FontProfile font = fontFactory.get("avenir:16.0");
        Element<?>[] windows = new Element<?>[PluginCategory.values().length];
        for (int i = 0; i < PluginCategory.values().length; i++) {
            PluginCategory category = PluginCategory.values()[i];
            windows[i] =
                    new ResizeDragPanel().setOffset(32, 32).setSize(200, 300).setBounds(200, 74, 200, 500).setPadding(5, 5, 5, 5).setHandleSize(5).add((window) -> new Element<?>[]{
                            new Label().setOffset(0, 0).setSize(window.getW(true), 32).setVerAlign(Label.Align.CENTER).setAnchor(true, true, true, false).setFontRenderer(font).setLabel(category.getDisplayName()).setColor(-1),
                            new Scrollable().setOffset(0, 32).setSize(window.getW(true), window.getH(true)).setAnchor(true).add((scrollable) -> new Element<?>[]{
                                    new ElementList().setOffset(0, 0).setSize(scrollable.getW(true), 0).setAnchor(true, true, true, false).add((elementList) -> {
                                        Set<Element<Button>> buttons = new HashSet<>();
                                        for (Plugin plugin : pluginFactory.getValues())
                                            buttons.add(new Button().setOffset(0, 0).setSize(elementList.getW(true), 32).setPadding(8, 0, 8, 0).setAnchor(true, true, true, false).add((button) -> new Element<?>[]{
                                                    new Label().setOffset(0, 0).setSize(button.getW(true), button.getH(true)).setVerAlign(Label.Align.CENTER).setAnchor(true).setFontRenderer(font).setLabel(plugin.getName()).setColor(-1)
                                            }));
                                        return buttons.toArray(new Element<?>[0]);
                                    })
                            })
                    });
        }

        add((ui) -> windows);
    }
}
