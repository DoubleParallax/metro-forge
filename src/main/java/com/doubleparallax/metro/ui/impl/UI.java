package com.doubleparallax.metro.ui.impl;

import com.doubleparallax.metro.ui.elements.Element;

public abstract class UI<T extends UI<T>> extends Element<T> {

    public UI() {
        setSize(mc.getMainWindow().getWidth(), mc.getMainWindow().getHeight());
    }

    public void init() {
        setSize(mc.getMainWindow().getWidth(), mc.getMainWindow().getHeight());
    }

}
