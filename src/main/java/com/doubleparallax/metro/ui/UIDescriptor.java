package com.doubleparallax.metro.ui;

import net.minecraft.client.gui.screen.Screen;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface UIDescriptor {

    Class<? extends Screen> value();

}
