package com.doubleparallax.metro.ui;

import com.doubleparallax.metro.Factory;
import com.doubleparallax.metro.component.Component;
import com.doubleparallax.metro.ui.impl.UI;
import net.minecraft.client.gui.screen.Screen;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;

@Component(priority = 11)
public class UIFactory extends Factory<Class<? extends Screen>, UI<?>> {

    private static final Logger LOGGER = LogManager.getLogger();

    public UIFactory() {
        LOGGER.debug("Populating UI factory map with subtypes of " + UI.class.getSimpleName());
        new Reflections()
                .getSubTypesOf(UI.class)
                .forEach((type) -> {
                    try {
                        UIDescriptor descriptor = type.getAnnotation(UIDescriptor.class);
                        if (descriptor == null) {
                            LOGGER.warn("UI descriptor not found for " + type.getSimpleName());
                            return;
                        }

                        LOGGER.debug("Initializing UI class " + type.getSimpleName());
                        put(descriptor.value(), type.getConstructor().newInstance());
                        LOGGER.debug("Initialized UI class " + type.getSimpleName());
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        LOGGER.error("There was an issue creating UI " + type.getSimpleName(), e);
                    }
                });
    }

}
