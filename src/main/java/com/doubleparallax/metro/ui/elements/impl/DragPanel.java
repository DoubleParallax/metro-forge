package com.doubleparallax.metro.ui.elements.impl;

import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.ui.elements.interaction.InteractionException;

public class DragPanel extends Element<DragPanel> {

    private double[] mouseFrom = new double[2];
    private boolean dragging;

    @Override
    public void mouseClicked(double mx, double my, int button) throws InteractionException {
        super.mouseClicked(mx, my, button);
        if (hover) {
            dragging = true;
            mouseFrom[LEFT] = mx - offset[LEFT];
            mouseFrom[TOP] = my - offset[TOP];
            throw new InteractionException();
        }
    }

    @Override
    public void mouseReleased(double mx, double my, int button) throws InteractionException {
        super.mouseReleased(mx, my, button);
        if (dragging)
            dragging = false;
    }

    @Override
    public void update() {
        if (dragging)
            setOffset(mc.mouseHelper.getMouseX() - mouseFrom[LEFT], mc.mouseHelper.getMouseY() - mouseFrom[TOP]);
        super.update();
    }

}
