package com.doubleparallax.metro.ui.elements.impl;

import com.doubleparallax.metro.ui.elements.interaction.InteractionException;
import org.lwjgl.glfw.GLFW;

public class ResizeDragPanel extends ResizePanel<ResizeDragPanel> {

    private boolean draggable;

    public boolean isDraggable() {
        return draggable;
    }

    public ResizeDragPanel setDraggable(boolean draggable) {
        this.draggable = draggable;
        return this;
    }

    @Override
    public void mouseClicked(double mx, double my, int button) throws InteractionException {
        super.mouseClicked(mx, my, button);
        if (draggable && button == GLFW.GLFW_MOUSE_BUTTON_LEFT) {
            if (hover) {
                dragging = true;
                mouseFrom[LEFT] = mx - offset[LEFT];
                mouseFrom[TOP] = my - offset[TOP];
                throw new InteractionException();
            }
        }
    }

    @Override
    public void mouseReleased(double mx, double my, int button) throws InteractionException {
        super.mouseReleased(mx, my, button);
        if (draggable && button == GLFW.GLFW_MOUSE_BUTTON_LEFT)
            if (dragging)
                dragging = false;
    }

    @Override
    public void update() {
        if (draggable && dragging && !(draggingSides[LEFT] || draggingSides[TOP] || draggingSides[RIGHT] || draggingSides[BOTTOM]))
            setOffset(mc.mouseHelper.getMouseX() - mouseFrom[LEFT], mc.mouseHelper.getMouseY() - mouseFrom[TOP]);
        super.update();
    }

}
