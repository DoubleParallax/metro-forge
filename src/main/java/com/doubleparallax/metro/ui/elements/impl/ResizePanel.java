package com.doubleparallax.metro.ui.elements.impl;

import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.ui.elements.interaction.InteractionException;
import com.doubleparallax.metro.util.MathHelper;
import org.lwjgl.glfw.GLFW;

public class ResizePanel<T extends ResizePanel<T>> extends Element<T> {

    private static final int MIN_WIDTH = 0;
    private static final int MIN_HEIGHT = 1;
    private static final int MAX_WIDTH = 2;
    private static final int MAX_HEIGHT = 3;

    protected final double[] mouseFrom = new double[4];
    protected final boolean[] hoverSides = new boolean[4];
    protected final boolean[] draggingSides = new boolean[4];
    private final double[] bounds = new double[]{0, 0, -1, -1};
    private double handleSize = 3;
    protected boolean resizable;

    protected boolean dragging;

    public T setBounds(double minW, double minH, double maxW, double maxH) {
        bounds[MIN_WIDTH] = minW;
        bounds[MIN_HEIGHT] = minH;
        bounds[MAX_WIDTH] = maxW;
        bounds[MAX_HEIGHT] = maxH;
        return self;
    }

    public T setHandleSize(double handleSize) {
        this.handleSize = handleSize;
        return self;
    }

    public T setResizable(boolean resizable) {
        this.resizable = resizable;
        return self;
    }

    @Override
    protected void updateHoveredState() {
        super.updateHoveredState();
        double mx = mc.mouseHelper.getMouseX();
        double my = mc.mouseHelper.getMouseY();
        hoverSides[LEFT] = resizable && (draggingSides[LEFT] || (hover && MathHelper.isInside(mx, my, x, y, Math.max(padding[LEFT], handleSize), h)));
        hoverSides[TOP] = resizable && (draggingSides[TOP] || (hover && MathHelper.isInside(mx, my, x, y, w, Math.max(padding[TOP], handleSize))));
        hoverSides[RIGHT] = resizable && (draggingSides[RIGHT] || (hover && MathHelper.isInside(mx, my, x + w - Math.max(padding[RIGHT], handleSize), y, Math.max(padding[RIGHT], handleSize), h)));
        hoverSides[BOTTOM] = resizable && (draggingSides[BOTTOM] || (hover && MathHelper.isInside(mx, my, x, y + h - Math.max(padding[BOTTOM], handleSize), w, Math.max(padding[BOTTOM], handleSize))));
    }

    @Override
    public void mouseClicked(double mx, double my, int button) throws InteractionException {
        super.mouseClicked(mx, my, button);
        if (resizable && button == GLFW.GLFW_MOUSE_BUTTON_LEFT) {
            if (hoverSides[LEFT]) draggingSides[LEFT] = true;
            if (hoverSides[TOP]) draggingSides[TOP] = true;
            if (hoverSides[RIGHT]) draggingSides[RIGHT] = true;
            if (hoverSides[BOTTOM]) draggingSides[BOTTOM] = true;
            if (hoverSides[LEFT] || hoverSides[TOP] || hoverSides[RIGHT] || hoverSides[BOTTOM]) {
                dragging = true;
                mouseFrom[LEFT] = mx - offset[LEFT];
                mouseFrom[TOP] = my - offset[TOP];
                mouseFrom[RIGHT] = mx - (offset[LEFT] + w);
                mouseFrom[BOTTOM] = my - (offset[TOP] + h);
                throw new InteractionException();
            }
        }
    }

    @Override
    public void mouseReleased(double mx, double my, int button) throws InteractionException {
        super.mouseReleased(mx, my, button);
        if (resizable && button == GLFW.GLFW_MOUSE_BUTTON_LEFT) {
            if (draggingSides[LEFT]) draggingSides[LEFT] = false;
            if (draggingSides[TOP]) draggingSides[TOP] = false;
            if (draggingSides[RIGHT]) draggingSides[RIGHT] = false;
            if (draggingSides[BOTTOM]) draggingSides[BOTTOM] = false;
            if (dragging) dragging = false;
        }
    }

    @Override
    public void update() {
        if (resizable && dragging) {
            double mx = mc.mouseHelper.getMouseX();
            double my = mc.mouseHelper.getMouseY();
            double minW = Math.max(bounds[MIN_WIDTH], -(padding[LEFT] + padding[RIGHT]));
            double minH = Math.max(bounds[MIN_HEIGHT], -(padding[BOTTOM] + padding[BOTTOM]));
            double maxW = bounds[MAX_WIDTH] == -1 ? Double.MAX_VALUE : Math.max(bounds[MAX_WIDTH], padding[LEFT] + padding[RIGHT]);
            double maxH = bounds[MAX_HEIGHT] == -1 ? Double.MAX_VALUE : Math.max(bounds[MAX_HEIGHT], padding[TOP] + padding[BOTTOM]);
            if (draggingSides[LEFT]) {
                double dif = mx - (offset[LEFT] + mouseFrom[LEFT]);
                dif = MathHelper.bound(dif, w - maxW, w - minW);
                offset[LEFT] += dif;
                w -= dif;
            }
            if (draggingSides[TOP]) {
                double dif = my - (offset[TOP] + mouseFrom[TOP]);
                dif = MathHelper.bound(dif, h - maxH, h - minH);
                offset[TOP] += dif;
                h -= dif;
            }
            if (draggingSides[RIGHT]) w = MathHelper.bound(mx - mouseFrom[RIGHT] - offset[LEFT], minW, maxW);
            if (draggingSides[BOTTOM]) h = MathHelper.bound(my - mouseFrom[BOTTOM] - offset[TOP], minH, maxH);
            setPosition(parent.getX() + parent.getPadding()[LEFT] + offset[LEFT] + margin[LEFT], parent.getY() + parent.getPadding()[TOP] + offset[TOP] + margin[TOP]);
        }
        super.update();
    }

}
