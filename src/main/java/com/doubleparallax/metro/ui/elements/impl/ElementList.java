package com.doubleparallax.metro.ui.elements.impl;

import com.doubleparallax.metro.ui.elements.Element;

import java.util.function.Function;

public class ElementList extends Element<ElementList> {

    private int horizontal;

    public boolean isHorizontal() {
        return horizontal == 1;
    }

    public ElementList setHorizontal(boolean horizontal) {
        this.horizontal = horizontal ? 1 : 0;
        return this;
    }

    @Override
    public ElementList setAnchor(boolean left, boolean top, boolean right, boolean bottom) {
        super.setAnchor(left, top, right, false);
        return this;
    }

    @Override
    public ElementList add(Function<ElementList, Element<?>[]> function) {
        Element<?>[] elements = function.apply(self);
        double offset = 0;
        for (Element<?> element : elements) {
            element.setOffset(isHorizontal() ? offset : element.getOffsetX(), isHorizontal() ? element.getOffsetY() : offset);
            offset+= isHorizontal() ? element.getW(false) : element.getContentHeight();
            this.elements.add(element.setParent(this));
        }
        if (isHorizontal()) w = offset;
        else h = offset;
        return self;
    }

    @Override
    public void updateChildren(double incX, double incY) {
        double offset = 0;
        for (Element<?> element : elements) {
            element.setOffset(isHorizontal() ? offset : element.getOffsetX(), isHorizontal() ? element.getOffsetY() : offset);
            offset+= isHorizontal() ? element.getW(false) : element.getContentHeight();
        }

        super.updateChildren(incX, incY);
    }

}
