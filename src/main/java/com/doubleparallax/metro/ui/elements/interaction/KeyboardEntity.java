package com.doubleparallax.metro.ui.elements.interaction;

public class KeyboardEntity {

    private final int modifiers;

    public KeyboardEntity(int modifiers) {
        this.modifiers = modifiers;
    }

    public int getModifiers() {
        return modifiers;
    }

    public static class Action extends KeyboardEntity {
        private final int key, scanCode;

        public Action(int key, int scanCode, int modifiers) {
            super(modifiers);

            this.key = key;
            this.scanCode = scanCode;
        }

        public int getKey() {
            return key;
        }

        public int getScanCode() {
            return scanCode;
        }
    }

    public static class CharTyped extends KeyboardEntity {
        private final char charTyped;

        public CharTyped(char charTyped, int modifiers) {
            super(modifiers);

            this.charTyped = charTyped;
        }

        public char getCharTyped() {
            return charTyped;
        }
    }

}
