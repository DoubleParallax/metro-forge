package com.doubleparallax.metro.ui.elements.impl;

import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.util.render.font.impl.FontProfile;

public class Label extends Element<Label> {

    public enum Align {
        START,
        CENTER,
        END
    }

    private FontProfile fontRenderer;
    private String label;
    private int color = 0xFF000000;
    private Align horAlign = Align.START,
            verAlign = Align.START;

    public FontProfile getFontRenderer() {
        return fontRenderer;
    }

    public Label setFontRenderer(FontProfile fontRenderer) {
        this.fontRenderer = fontRenderer;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public Label setLabel(String label) {
        this.label = label;
        return this;
    }

    public int getColor() {
        return color;
    }

    public Label setColor(int color) {
        this.color = color;
        return this;
    }

    public Align getHorAlign() {
        return horAlign;
    }

    public Label setHorAlign(Align horAlign) {
        this.horAlign = horAlign;
        return this;
    }

    public Align getVerAlign() {
        return verAlign;
    }

    public Label setVerAlign(Align verAlign) {
        this.verAlign = verAlign;
        return this;
    }
}
