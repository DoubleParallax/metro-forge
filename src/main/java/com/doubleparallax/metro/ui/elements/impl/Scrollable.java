package com.doubleparallax.metro.ui.elements.impl;

import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.ui.elements.interaction.InteractionException;
import com.doubleparallax.metro.util.MathHelper;

public class Scrollable extends Element<Scrollable> {

    private double scroll;
    private int horizontal;

    public boolean isHorizontal() {
        return horizontal == 1;
    }

    public Scrollable setHorizontal(boolean horizontal) {
        this.horizontal = horizontal ? 1 : 0;
        return this;
    }

    @Override
    public void mouseScrolled(double mx, double my, double scroll) throws InteractionException {
        super.mouseScrolled(mx, my, scroll);
        if (hover) {
            this.scroll += scroll * 20;
            this.scroll = MathHelper.bound(this.scroll, -Math.max(contentHeight - h, 0), 0);
            throw new InteractionException();
        }
    }

    @Override
    public void update() {
        updateHoveredState();
        updateChildren(scroll * horizontal, scroll * (-horizontal + 1));
        this.scroll = MathHelper.bound(this.scroll, -Math.max(contentHeight - h, 0), 0);
    }

}
