package com.doubleparallax.metro.ui.elements;

import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.ui.UIInitializeException;
import com.doubleparallax.metro.ui.elements.interaction.Interaction;
import com.doubleparallax.metro.ui.elements.interaction.InteractionException;
import com.doubleparallax.metro.ui.elements.interaction.KeyboardEntity;
import com.doubleparallax.metro.ui.elements.interaction.MouseEntity;
import com.doubleparallax.metro.ui.theme.Theme;
import com.doubleparallax.metro.ui.theme.ThemeFactory;
import com.doubleparallax.metro.ui.theme.ThemeRendererFactory;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElement;
import com.doubleparallax.metro.util.MathHelper;
import com.doubleparallax.metro.util.MinecraftHelper;
import com.mojang.blaze3d.matrix.MatrixStack;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public abstract class Element<T extends Element<T>> implements MinecraftHelper {

    private static final Logger LOGGER = LogManager.getLogger();

    protected static final int LEFT = 0;
    protected static final int TOP = 1;
    protected static final int RIGHT = 2;
    protected static final int BOTTOM = 3;

    protected final T self;
    protected final ThemeElement<T> renderer;

    protected double x, y, w, h;
    protected double contentHeight;
    protected final double[] offset = new double[4];
    protected final double[] padding = new double[4];
    protected final double[] margin = new double[4];
    protected final boolean[] anchor = new boolean[4];
    protected boolean hover;
    protected final List<Element<?>> elements = new ArrayList<>();
    protected Element<?> parent;

    private Interaction<T, MouseEntity.Action> onMouseClicked;
    private Interaction<T, MouseEntity.Action> onMouseReleased;
    private Interaction<T, MouseEntity.Scrolled> onMouseScrolled;
    private Interaction<T, KeyboardEntity.Action> onKeyPressed;
    private Interaction<T, KeyboardEntity.Action> onKeyReleased;
    private Interaction<T, KeyboardEntity.CharTyped> onCharTyped;

    protected Element() {
        try {
            Class<T> type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            self = type.cast(this);

            try {
                ThemeFactory themeFactory = ComponentFactory.INSTANCE.getComponent(ThemeFactory.class);
                ThemeRendererFactory<ThemeElement<T>> themeRendererFactory = (ThemeRendererFactory<ThemeElement<T>>) themeFactory.get(Theme.METRO);
                renderer = themeRendererFactory.get(type);
                if (renderer == null)
                    LOGGER.error(String.format("Failed to find theme renderer for element %s, should crash for this but leaving as an error log for debugging", type.getSimpleName()));
            } catch (ClassCastException e) {
                throw new UIInitializeException("Failed to get theme renderer for element");
            }
        } catch (ClassCastException e) {
            throw new UIInitializeException("Type generics does not match " + getClass().getSimpleName());
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getW(boolean padding) {
        return w - (padding ? (this.padding[LEFT] + this.padding[RIGHT]) : 0);
    }

    public double getH(boolean padding) {
        return h - (padding ? (this.padding[TOP] + this.padding[BOTTOM]) : 0);
    }

    public double getOffsetX() {
        return offset[LEFT];
    }

    public double getOffsetY() {
        return offset[TOP];
    }

    public double getContentHeight() {
        return Math.max(h, contentHeight);
    }

    public void setPosition(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public T setOffset(double left, double top) {
        offset[LEFT] = left;
        offset[TOP] = top;
        return self;
    }

    public T setSize(double w, double h) {
        this.w = w;
        this.h = h;
        return self;
    }

    public double[] getPadding() {
        return padding;
    }

    public T setPadding(double padding) {
        return setPadding(padding, padding, padding, padding);
    }

    public T setPadding(double left, double top, double right, double bottom) {
        padding[LEFT] = left;
        padding[TOP] = top;
        padding[RIGHT] = right;
        padding[BOTTOM] = bottom;
        return self;
    }

    public double[] getMargin() {
        return margin;
    }

    public T setMargin(double margin) {
        return setMargin(margin, margin, margin, margin);
    }

    public T setMargin(double left, double top, double right, double bottom) {
        margin[LEFT] = left;
        margin[TOP] = top;
        margin[RIGHT] = right;
        margin[BOTTOM] = bottom;
        return self;
    }

    public boolean[] getAnchor() {
        return anchor;
    }

    public T setAnchor(boolean anchor) {
        return setAnchor(anchor, anchor, anchor, anchor);
    }

    public T setAnchor(boolean left, boolean top, boolean right, boolean bottom) {
        anchor[LEFT] = left;
        anchor[TOP] = top;
        anchor[RIGHT] = right;
        anchor[BOTTOM] = bottom;
        return self;
    }

    public T setParent(Element<?> parent) {
        this.parent = parent;

        offset[RIGHT] = parent.w - w;
        offset[BOTTOM] = parent.h - h;

        return self;
    }

    public T setOnMouseClicked(Interaction<T, MouseEntity.Action> onMouseClicked) {
        this.onMouseClicked = onMouseClicked;
        return self;
    }

    public T setOnMouseReleased(Interaction<T, MouseEntity.Action> onMouseReleased) {
        this.onMouseReleased = onMouseReleased;
        return self;
    }

    public T setOnMouseScrolled(Interaction<T, MouseEntity.Scrolled> onMouseScrolled) {
        this.onMouseScrolled = onMouseScrolled;
        return self;
    }

    public T setOnKeyPressed(Interaction<T, KeyboardEntity.Action> onKeyPressed) {
        this.onKeyPressed = onKeyPressed;
        return self;
    }

    public T setOnKeyReleased(Interaction<T, KeyboardEntity.Action> onKeyReleased) {
        this.onKeyReleased = onKeyReleased;
        return self;
    }

    public T setOnCharTyped(Interaction<T, KeyboardEntity.CharTyped> onCharTyped) {
        this.onCharTyped = onCharTyped;
        return self;
    }

    public T add(Function<T, Element<?>[]> function) {
        Element<?>[] elements = function.apply(self);
        for (Element<?> element : elements)
            this.elements.add(element.setParent(this));
        return self;
    }

    public void mouseClicked(double mx, double my, int button) throws InteractionException {
        if (onMouseClicked != null)
            onMouseClicked.accept(self, new MouseEntity.Action(mx, my, button));

        for (Element<?> element : elements)
            element.mouseClicked(mx, my, button);
    }

    public void mouseReleased(double mx, double my, int button) throws InteractionException {
        if (onMouseReleased != null)
            onMouseReleased.accept(self, new MouseEntity.Action(mx, my, button));

        for (Element<?> element : elements)
            element.mouseReleased(mx, my, button);
    }

    public void mouseScrolled(double mx, double my, double scroll) throws InteractionException {
        if (onMouseScrolled != null)
            onMouseScrolled.accept(self, new MouseEntity.Scrolled(mx, my, scroll));

        for (Element<?> element : elements)
            element.mouseScrolled(mx, my, scroll);
    }

    public void keyPressed(int key, int scanCode, int modifiers) throws InteractionException {
        if (onKeyPressed != null)
            onKeyPressed.accept(self, new KeyboardEntity.Action(key, scanCode, modifiers));

        for (Element<?> element : elements)
            element.keyPressed(key, scanCode, modifiers);
    }

    public void keyReleased(int key, int scanCode, int modifiers) throws InteractionException {
        if (onKeyReleased != null)
            onKeyReleased.accept(self, new KeyboardEntity.Action(key, scanCode, modifiers));

        for (Element<?> element : elements)
            element.keyReleased(key, scanCode, modifiers);
    }

    public void charTyped(char charTyped, int modifiers) throws InteractionException {
        if (onCharTyped != null)
            onCharTyped.accept(self, new KeyboardEntity.CharTyped(charTyped, modifiers));

        for (Element<?> element : elements)
            element.charTyped(charTyped, modifiers);
    }

    public boolean isHovered() {
        return hover;
    }

    protected void updateHoveredState() {
        this.hover = (parent == null || parent.isHovered()) && MathHelper.isInside(mc.mouseHelper.getMouseX(), mc.mouseHelper.getMouseY(), x, y, w, h);
    }

    public void update() {
        updateHoveredState();
        updateChildren(0, 0);
    }

    protected void updateChildren(double incX, double incY) {
        double top = Double.MAX_VALUE;
        double bottom = Double.MIN_VALUE;
        for (Element<?> element : elements) {
            if (element.anchor[LEFT])           element.x = x + padding[LEFT] + element.offset[LEFT] + element.margin[LEFT];
            else if (element.anchor[RIGHT])     element.x = x + padding[LEFT] - element.offset[RIGHT] - element.margin[RIGHT] + w - element.w;
            if (element.anchor[LEFT] && element.anchor[RIGHT])
                element.w = w - element.offset[LEFT] - element.offset[RIGHT] - element.margin[RIGHT];

            if (element.anchor[TOP])            element.y = y + padding[TOP] + element.offset[TOP] + element.margin[TOP];
            else if (element.anchor[BOTTOM])    element.y = y + padding[TOP] - element.offset[BOTTOM] - element.margin[BOTTOM] + h - element.h;
            if (element.anchor[TOP] && element.anchor[BOTTOM])
                element.h = h - element.offset[TOP] - element.offset[BOTTOM] - element.margin[BOTTOM];

            if (!(element.anchor[LEFT] || element.anchor[TOP] || element.anchor[RIGHT] || element.anchor[BOTTOM]))
                element.setPosition(element.offset[LEFT], element.offset[TOP]);

            element.x+= incX;
            element.y+= incY;

            element.update();

            top = Math.min(top, element.getOffsetY());
            bottom = Math.max(bottom, element.getOffsetY() + element.getContentHeight());
        }
        contentHeight = bottom - top;
    }

    public void render(MatrixStack matrixStack) {
        if (renderer != null)
            renderer.preRender(matrixStack, self);
        for (int i = elements.size() - 1; i >= 0; i--)
            elements.get(i).render(matrixStack);
        if (renderer != null)
            renderer.postRender(matrixStack, self);
    }

}
