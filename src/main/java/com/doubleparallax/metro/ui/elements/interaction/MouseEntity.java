package com.doubleparallax.metro.ui.elements.interaction;

public class MouseEntity {

    private final double mx, my;

    public MouseEntity(double mx, double my) {
        this.mx = mx;
        this.my = my;
    }

    public double getMx() {
        return mx;
    }

    public double getMy() {
        return my;
    }

    public static class Action extends MouseEntity {
        private final int button;

        public Action(double mx, double my, int button) {
            super(mx, my);

            this.button = button;
        }

        public int getButton() {
            return button;
        }

        public static class Dragged extends Action {
            private final double dx, dy;

            public Dragged(double mx, double my, int button, double dx, double dy) {
                super(mx, my, button);

                this.dx = dx;
                this.dy = dy;
            }

            public double getDx() {
                return dx;
            }

            public double getDy() {
                return dy;
            }
        }
    }

    public static class Scrolled extends MouseEntity {
        private final double scroll;

        public Scrolled(double mx, double my, double scroll) {
            super(mx, my);

            this.scroll = scroll;
        }

        public double getScroll() {
            return scroll;
        }
    }

}
