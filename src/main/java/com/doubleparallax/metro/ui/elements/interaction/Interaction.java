package com.doubleparallax.metro.ui.elements.interaction;

public interface Interaction<T, U> {

    void accept(T t, U u) throws InteractionException;

}
