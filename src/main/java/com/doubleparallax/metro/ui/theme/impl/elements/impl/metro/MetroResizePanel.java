package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.elements.impl.ResizePanel;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.mojang.blaze3d.matrix.MatrixStack;

@ThemeElementDescriptor(ResizePanel.class)
public class MetroResizePanel implements MetroThemeElement<ResizePanel<?>> {

    @Override
    public void preRender(MatrixStack matrixStack, ResizePanel<?> element) {
    }

    @Override
    public void postRender(MatrixStack matrixStack, ResizePanel<?> element) {
    }

}
