package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.elements.impl.Button;
import com.doubleparallax.metro.ui.elements.impl.IconButton;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.doubleparallax.metro.util.render.geometry.Circle2D;
import com.doubleparallax.metro.util.render.geometry.Rect2D;
import com.mojang.blaze3d.matrix.MatrixStack;

@ThemeElementDescriptor(IconButton.class)
public class MetroIconButton implements MetroThemeElement<IconButton> {

    @Override
    public void preRender(MatrixStack matrixStack, IconButton element) {
        Circle2D.Data data = new Circle2D.Data().setPos(element.getX(), element.getY(), element.getX() + element.getW(false), element.getY() + element.getH(false)).setAlpha(element.isHovered() ? 1 : 0.5f).setUrl("https://cdn.discordapp.com/avatars/275214232885395457/56315ad7ee435b6269afeac4bcccc8be.png?size=128");
        Circle2D.draw(matrixStack, data);
    }

    @Override
    public void postRender(MatrixStack matrixStack, IconButton element) {
    }

}
