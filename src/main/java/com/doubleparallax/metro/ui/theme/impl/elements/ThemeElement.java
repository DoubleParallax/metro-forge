package com.doubleparallax.metro.ui.theme.impl.elements;

import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.util.MinecraftHelper;
import com.mojang.blaze3d.matrix.MatrixStack;

public interface ThemeElement<T extends Element<?>> extends MinecraftHelper {

    void preRender(MatrixStack matrixStack, T element);

    void postRender(MatrixStack matrixStack, T element);

}
