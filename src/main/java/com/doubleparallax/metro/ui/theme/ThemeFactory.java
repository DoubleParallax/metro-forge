package com.doubleparallax.metro.ui.theme;

import com.doubleparallax.metro.Factory;
import com.doubleparallax.metro.component.Component;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;

@Component(priority = 5)
public class ThemeFactory extends Factory<Theme, ThemeRendererFactory<? extends ThemeElement<?>>> {

    private static final Logger LOGGER = LogManager.getLogger();

    public ThemeFactory() {
        LOGGER.debug("Populating theme factory map with subtypes of " + ThemeRendererFactory.class.getSimpleName());
        new Reflections()
                .getSubTypesOf(ThemeRendererFactory.class)
                .forEach((type) -> {
                    try {
                        ThemeRendererDescriptor descriptor = type.getAnnotation(ThemeRendererDescriptor.class);
                        if (descriptor == null) {
                            LOGGER.warn("Theme renderer descriptor not found for " + type.getSimpleName());
                            return;
                        }

                        LOGGER.debug("Initializing theme renderer class " + type.getSimpleName());
                        put(descriptor.value(), type.getConstructor().newInstance());
                        LOGGER.debug("Initialized theme renderer class " + type.getSimpleName());
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        LOGGER.error("There was an issue creating theme renderer " + type.getSimpleName(), e);
                    }
                });
    }

}
