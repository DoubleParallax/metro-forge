package com.doubleparallax.metro.ui.theme.impl;

import com.doubleparallax.metro.ui.theme.Theme;
import com.doubleparallax.metro.ui.theme.ThemeRendererDescriptor;
import com.doubleparallax.metro.ui.theme.ThemeRendererFactory;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;

@ThemeRendererDescriptor(Theme.METRO)
public class MetroThemeRendererFactory extends ThemeRendererFactory<MetroThemeElement<?>> {
}
