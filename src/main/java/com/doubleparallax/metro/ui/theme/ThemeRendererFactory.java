package com.doubleparallax.metro.ui.theme;

import com.doubleparallax.metro.Factory;
import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElement;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;

public abstract class ThemeRendererFactory<T extends ThemeElement<?>> extends Factory<Class<? extends Element>, T> {

    private static final Logger LOGGER = LogManager.getLogger();

    public ThemeRendererFactory() {
        LOGGER.debug("Populating theme renderer factory map with subtypes of " + ThemeElement.class.getSimpleName());
        new Reflections()
                .getSubTypesOf(ThemeElement.class)
                .forEach((type) -> {
                    try {
                        ThemeElementDescriptor descriptor = type.getAnnotation(ThemeElementDescriptor.class);
                        if (descriptor == null) {
                            LOGGER.warn("Theme element descriptor not found for " + type.getSimpleName());
                            return;
                        }

                        LOGGER.debug("Initializing theme element class " + type.getSimpleName());
                        T inst = (T) type.getConstructor().newInstance();
                        for (Class<? extends Element> cls : descriptor.value())
                            put(cls, inst);
                        LOGGER.debug("Initialized theme element class " + type.getSimpleName());
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        LOGGER.error("There was an issue creating theme element " + type.getSimpleName(), e);
                    }
                });
    }

}
