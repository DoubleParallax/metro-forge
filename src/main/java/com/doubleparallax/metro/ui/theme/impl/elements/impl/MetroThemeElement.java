package com.doubleparallax.metro.ui.theme.impl.elements.impl;

import com.doubleparallax.metro.ui.elements.Element;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElement;

public interface MetroThemeElement<T extends Element<?>> extends ThemeElement<T> {
}
