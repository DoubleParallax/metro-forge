package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.elements.impl.Scrollable;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.doubleparallax.metro.util.render.geometry.Rect2D;
import com.mojang.blaze3d.matrix.MatrixStack;
import org.lwjgl.opengl.GL11;

@ThemeElementDescriptor(Scrollable.class)
public class MetroScrollable implements MetroThemeElement<Scrollable> {

    @Override
    public void preRender(MatrixStack matrixStack, Scrollable element) {
        Rect2D.Data data = new Rect2D.Data().setPos(element.getX(), element.getY(), element.getX() + element.getW(false), element.getY() + element.getH(false)).setColor(0xFF262526);
        Rect2D.draw(matrixStack, data);

        int displayHeight = mc.getMainWindow().getHeight();
        GL11.glScissor((int) Math.round(element.getX()), displayHeight - (int) Math.round(element.getY() + element.getH(false)), (int) Math.round(element.getW(false)), (int) Math.round(element.getH(false)));
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
    }

    @Override
    public void postRender(MatrixStack matrixStack, Scrollable element) {
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

}
