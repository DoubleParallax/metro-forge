package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.elements.impl.Label;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.mojang.blaze3d.matrix.MatrixStack;

@ThemeElementDescriptor(Label.class)
public class MetroLabel implements MetroThemeElement<Label> {

    private double getAlignmentPosition(double start, double end, double size, Label.Align align) {
        switch (align) {
            case CENTER: return start + (end / 2.) - (size / 2.);
            case END: return end - size;
        }
        return start;
    }

    @Override
    public void preRender(MatrixStack matrixStack, Label element) {
        double x = getAlignmentPosition(element.getX(), element.getW(false), element.getFontRenderer().getWidth(element.getLabel()), element.getHorAlign());
        double y = getAlignmentPosition(element.getY(), element.getH(false), element.getFontRenderer().getHeight(element.getLabel()), element.getVerAlign());

        element.getFontRenderer().drawString(matrixStack, element.getLabel(), x, y, element.getColor());
    }

    @Override
    public void postRender(MatrixStack matrixStack, Label element) {
    }

}
