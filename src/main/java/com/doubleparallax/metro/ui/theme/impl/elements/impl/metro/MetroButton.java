package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.elements.impl.Button;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.doubleparallax.metro.util.render.geometry.Rect2D;
import com.mojang.blaze3d.matrix.MatrixStack;

@ThemeElementDescriptor(Button.class)
public class MetroButton implements MetroThemeElement<Button> {

    @Override
    public void preRender(MatrixStack matrixStack, Button element) {
        Rect2D.Data data = new Rect2D.Data().setPos(element.getX(), element.getY(), element.getX() + element.getW(false), element.getY() + element.getH(false)).setColor(0xFFC34737).setAlpha(element.isHovered() ? 1 : 0.5f);
        Rect2D.draw(matrixStack, data);
    }

    @Override
    public void postRender(MatrixStack matrixStack, Button element) {
    }

}
