package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.impl.UI;
import com.doubleparallax.metro.ui.impl.UIDiscord;
import com.doubleparallax.metro.ui.impl.UIModMenu;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.doubleparallax.metro.util.render.geometry.Rect2D;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.util.ResourceLocation;

@ThemeElementDescriptor({UIModMenu.class, UIDiscord.class})
public class MetroUI implements MetroThemeElement<UI<?>> {

    public static final ResourceLocation MENU_BACKGROUND = new ResourceLocation("metro", "textures/menu_background.png");

    @Override
    public void preRender(MatrixStack matrixStack, UI<?> element) {
        if (mc.world == null)
            Rect2D.draw(matrixStack, new Rect2D.Data().setResource(MENU_BACKGROUND).setPos(0, 0, element.getW(false), element.getH(false)));
        else
            Rect2D.draw(matrixStack, new Rect2D.Data().setPos(element.getX(), element.getY(), element.getW(false), element.getH(false)).setColor(0xFF000000).setAlpha(0.7f));
    }

    @Override
    public void postRender(MatrixStack matrixStack, UI<?> element) {
    }

}
