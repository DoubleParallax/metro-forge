package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.elements.impl.DragPanel;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.mojang.blaze3d.matrix.MatrixStack;

@ThemeElementDescriptor(DragPanel.class)
public class MetroDragPanel implements MetroThemeElement<DragPanel> {

    @Override
    public void preRender(MatrixStack matrixStack, DragPanel element) {
    }

    @Override
    public void postRender(MatrixStack matrixStack, DragPanel element) {
    }

}
