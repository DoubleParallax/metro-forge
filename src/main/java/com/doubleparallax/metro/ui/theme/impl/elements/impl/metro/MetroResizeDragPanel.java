package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.elements.impl.ResizeDragPanel;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.doubleparallax.metro.util.render.geometry.Rect2D;
import com.mojang.blaze3d.matrix.MatrixStack;

@ThemeElementDescriptor(ResizeDragPanel.class)
public class MetroResizeDragPanel implements MetroThemeElement<ResizeDragPanel> {

    @Override
    public void preRender(MatrixStack matrixStack, ResizeDragPanel element) {
        Rect2D.Data data = new Rect2D.Data().setPos(element.getX(), element.getY(), element.getX() + element.getW(false), element.getY() + element.getH(false)).setColor(0xFF262526).setAlpha(0.7f);
        Rect2D.draw(matrixStack, data);
        Rect2D.draw(matrixStack, data.incrementPos(1, 1, -1, -1));
    }

    @Override
    public void postRender(MatrixStack matrixStack, ResizeDragPanel element) {
    }

}
