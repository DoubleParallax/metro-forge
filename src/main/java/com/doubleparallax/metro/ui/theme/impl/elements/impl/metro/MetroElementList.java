package com.doubleparallax.metro.ui.theme.impl.elements.impl.metro;

import com.doubleparallax.metro.ui.elements.impl.ElementList;
import com.doubleparallax.metro.ui.theme.impl.elements.ThemeElementDescriptor;
import com.doubleparallax.metro.ui.theme.impl.elements.impl.MetroThemeElement;
import com.doubleparallax.metro.util.render.geometry.Rect2D;
import com.mojang.blaze3d.matrix.MatrixStack;

@ThemeElementDescriptor(ElementList.class)
public class MetroElementList implements MetroThemeElement<ElementList> {

    @Override
    public void preRender(MatrixStack matrixStack, ElementList element) {
    }

    @Override
    public void postRender(MatrixStack matrixStack, ElementList element) {
    }

}
