package com.doubleparallax.metro.ui;

public class UIInitializeException extends RuntimeException {

    public UIInitializeException(String msg) {
        super(msg);
    }

    public UIInitializeException(String msg, Throwable exception) {
        super(msg, exception);
    }

}
