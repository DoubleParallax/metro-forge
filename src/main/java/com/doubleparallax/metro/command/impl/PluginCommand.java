package com.doubleparallax.metro.command.impl;

import com.doubleparallax.metro.attribute.Attribute;
import com.doubleparallax.metro.command.Command;
import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.plugin.Plugin;
import com.doubleparallax.metro.plugin.PluginFactory;
import com.doubleparallax.metro.plugin.impl.Commands;
import com.doubleparallax.metro.util.ChatHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PluginCommand extends Command {

    private static final Logger LOGGER = LogManager.getLogger();

    private final Plugin plugin;

    public PluginCommand(Plugin plugin) {
        this.plugin = plugin;
        initialize(plugin.getId(), plugin.getName(), plugin.getDesc(), plugin.getAlias());
    }

    @Override
    public void execute(String[] args) {
        if (args.length == 0) {
            Attribute attribute = plugin.get("active");
            if (attribute == null) {
                LOGGER.debug("Attribute active not found");
                return;
            }
            LOGGER.debug((plugin.active ? "Disabling" : "Enabling") + " plugin " + plugin.getId());

            boolean value = !plugin.active;
            if (!attribute.setFieldValue(value))
                LOGGER.warn(String.format("Failed to set attribute %s value to %s", attribute.getId(), value));
            else ChatHelper.sendChatMessage("%s %s set to %s", plugin.getName(), attribute.getLongName(), value);

            return;
        }

        PluginFactory pluginFactory = ComponentFactory.INSTANCE.getComponent(PluginFactory.class);
        Commands commands = (Commands) pluginFactory.get(Commands.class);

        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (!arg.startsWith(commands.argPrefix)) {
                LOGGER.debug("Argument value provided without an argument, ignoring");
                ChatHelper.sendChatMessage("Must provide an argument before assigning a value");
                continue;
            }
            arg = arg.substring(commands.argPrefix.length());
            Attribute attribute = plugin.get(arg);
            if (attribute == null) {
                LOGGER.warn("Argument " + arg + " not found for plugin " + plugin.getId());
                ChatHelper.sendChatMessage("Attribute '%s' not found", arg);
                continue;
            }
            if (i >= args.length - 1 || args[i + 1].startsWith(commands.argPrefix)) {
                LOGGER.debug("No value for argument, displaying value: " + attribute.getFieldValue());
                ChatHelper.sendChatMessage("%s %s: %s", plugin.getName(), attribute.getLongName(), attribute.getFieldValue());
                continue;
            }
            Object value = attribute.parseStringValue(args[++i]);
            if (!attribute.setFieldValue(value))
                LOGGER.warn(String.format("Failed to set attribute %s value to %s (%s)", attribute.getId(), value, args[i]));
            else ChatHelper.sendChatMessage("%s %s set to %s", plugin.getName(), attribute.getLongName(), value);
        }
    }

}
