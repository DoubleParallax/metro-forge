package com.doubleparallax.metro.command;

import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.util.Locale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Command {

    private static final Logger LOGGER = LogManager.getLogger();

    private final CommandFactory commandFactory = ComponentFactory.INSTANCE.getComponent(CommandFactory.class);
    private String id, name, desc;
    private String[] alias;

    protected Command() {
        CommandDescriptor descriptor = getClass().getAnnotation(CommandDescriptor.class);
        if (descriptor == null) {
            LOGGER.warn("No descriptor found for command " + getClass().getSimpleName());
            return;
        }
        initialize(descriptor.value(),
                Locale.translate(String.format("command.%s.name", getId())),
                Locale.translate(String.format("command.%s.desc", getId())),
                Locale.translate(String.format("command.%s.alias", getId())).split(","));
    }

    protected void initialize(String id, String name, String desc, String[] alias) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.alias = alias;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public String[] getAlias() {
        return alias;
    }

    public abstract void execute(String[] args);

}
