package com.doubleparallax.metro.command;

import com.doubleparallax.metro.Factory;
import com.doubleparallax.metro.component.Component;
import com.doubleparallax.metro.util.ChatHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;

@Component(priority = 5)
public class CommandFactory extends Factory<String[], Command> {

    private static final Logger LOGGER = LogManager.getLogger();

    public CommandFactory() {
        LOGGER.debug("Populating command factory map with subtypes of " + Command.class.getSimpleName());
        new Reflections()
                .getSubTypesOf(Command.class)
                .forEach((type) -> {
                    if (!type.isAnnotationPresent(CommandDescriptor.class)) {
                        LOGGER.debug("No command descriptor found for " + type.getSimpleName());
                        return;
                    }
                    try {
                        LOGGER.debug("Initializing command class " + type.getSimpleName());
                        Command command = type.getConstructor().newInstance();
                        put(command.getAlias(), command);
                        LOGGER.debug("Initialized command class " + type.getSimpleName());
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        LOGGER.error("There was an issue creating command " + type.getSimpleName(), e);
                    }
                });
    }

    private Command getCommandFromAlias(String command) {
        for (String[] keys : getKeys())
            for (String key : keys)
                if (key.equalsIgnoreCase(command))
                    return get(keys);
        return null;
    }

    public boolean execute(String[] args) {
        if (args.length == 0) return false;

        String baseCommand = args[0];
        Command command = getCommandFromAlias(baseCommand);
        if (command == null) {
            LOGGER.info("Command " + baseCommand + " not found");
            ChatHelper.sendChatMessage("Command '%s' not found", baseCommand);
            return true;
        }

        String[] newArgs = new String[args.length - 1];
        System.arraycopy(args, 1, newArgs, 0, newArgs.length);
        command.execute(newArgs);

        return true;
    }

}
