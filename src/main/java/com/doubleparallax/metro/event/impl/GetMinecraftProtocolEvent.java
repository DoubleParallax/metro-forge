package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;

public class GetMinecraftProtocolEvent extends Event<GetMinecraftProtocolEvent> {

    private int protocol;

    public int getProtocol() {
        return protocol;
    }

    public GetMinecraftProtocolEvent setProtocol(int protocol) {
        this.protocol = protocol;
        return this;
    }

}
