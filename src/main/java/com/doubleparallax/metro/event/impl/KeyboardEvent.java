package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventCreateException;

public abstract class KeyboardEvent<T extends KeyboardEvent<T>> extends Event<T> {

    protected final T self;

    protected KeyboardEvent(Class<T> type) {
        if (!type.isInstance(this))
            throw new EventCreateException("Type generics " + type.getSimpleName() + " does not match " + getClass().getSimpleName());
        self = type.cast(this);
    }

    public static abstract class Action<T extends Action<T>> extends KeyboardEvent<T> {
        private int keyCode, scanCode, modifiers;

        protected Action(Class<T> type) {
            super(type);
        }

        public int getKeyCode() {
            return keyCode;
        }

        public T setKeyCode(int keyCode) {
            this.keyCode = keyCode;
            return self;
        }

        public int getScanCode() {
            return scanCode;
        }

        public T setScanCode(int scanCode) {
            this.scanCode = scanCode;
            return self;
        }

        public int getModifiers() {
            return modifiers;
        }

        public T setModifiers(int modifiers) {
            this.modifiers = modifiers;
            return self;
        }

        public static class Pressed extends Action<Pressed> {
            public Pressed() {
                super(Pressed.class);
            }
        }

        public static class Released extends Action<Released> {
            public Released() {
                super(Released.class);
            }
        }
    }

    public static class CharTyped extends KeyboardEvent<CharTyped> {
        private char charTyped;
        private int modifiers;

        public CharTyped() {
            super(CharTyped.class);
        }

        public char getCharTyped() {
            return charTyped;
        }

        public CharTyped setCharTyped(char charTyped) {
            this.charTyped = charTyped;
            return this;
        }

        public int getModifiers() {
            return modifiers;
        }

        public CharTyped setModifiers(int modifiers) {
            this.modifiers = modifiers;
            return this;
        }
    }
}
