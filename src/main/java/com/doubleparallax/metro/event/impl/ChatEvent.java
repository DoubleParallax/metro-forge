package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;

public class ChatEvent extends Event<ChatEvent> {

    private String message;

    public String getMessage() {
        return message;
    }

    public ChatEvent setMessage(String message) {
        this.message = message;
        return this;
    }

}
