package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventCreateException;

public abstract class MouseEvent<T extends MouseEvent<T>> extends Event<T> {

    protected final T self;
    private double mx, my;

    protected MouseEvent(Class<T> type) {
        if (!type.isInstance(this))
            throw new EventCreateException("Type generics " + type.getSimpleName() + " does not match " + getClass().getSimpleName());
        self = type.cast(this);
    }

    public double getMx() {
        return mx;
    }

    public T setMx(double mx) {
        this.mx = mx;
        return self;
    }

    public double getMy() {
        return my;
    }

    public T setMy(double my) {
        this.my = my;
        return self;
    }

    public static abstract class Action<T extends Action<T>> extends MouseEvent<T> {
        private int button;

        protected Action(Class<T> type) {
            super(type);
        }

        public int getButton() {
            return button;
        }

        public T setButton(int button) {
            this.button = button;
            return self;
        }

        public static class Pressed extends Action<Pressed> {
            public Pressed() {
                super(Pressed.class);
            }
        }

        public static class Released extends Action<Released> {
            public Released() {
                super(Released.class);
            }
        }
    }

    public static class Scrolled extends MouseEvent<Scrolled> {
        private double scroll;

        public Scrolled() {
            super(Scrolled.class);
        }

        public double getScroll() {
            return scroll;
        }

        public Scrolled setScroll(double scroll) {
            this.scroll = scroll;
            return this;
        }
    }
}
