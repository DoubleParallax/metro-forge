package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.gui.screen.Screen;

public abstract class ScreenEvent<T extends ScreenEvent<T>> extends Event<T> {

    public static class Open extends ScreenEvent<Open> {
        private Screen oldScreen, newScreen;

        public Screen getOldScreen() {
            return oldScreen;
        }

        public Open setOldScreen(Screen oldScreen) {
            this.oldScreen = oldScreen;
            return this;
        }

        public Screen getNewScreen() {
            return newScreen;
        }

        public Open setNewScreen(Screen newScreen) {
            this.newScreen = newScreen;
            return this;
        }
    }

    public static class Render extends ScreenEvent<Render> {
        private double mx, my, renderPartialTicks;
        private MatrixStack matrixStack;

        public double getMx() {
            return mx;
        }

        public Render setMx(double mx) {
            this.mx = mx;
            return this;
        }

        public double getMy() {
            return my;
        }

        public Render setMy(double my) {
            this.my = my;
            return this;
        }

        public double getRenderPartialTicks() {
            return renderPartialTicks;
        }

        public Render setRenderPartialTicks(double renderPartialTicks) {
            this.renderPartialTicks = renderPartialTicks;
            return this;
        }

        public MatrixStack getMatrixStack() {
            return matrixStack;
        }

        public Render setMatrixStack(MatrixStack matrixStack) {
            this.matrixStack = matrixStack;
            return this;
        }
    }
}
