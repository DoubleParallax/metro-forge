package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;

public class LivingEntityUpdateEvent extends Event<LivingEntityUpdateEvent> {

    private Entity entity;
    private LivingEntity livingEntity;

    public Entity getEntity() {
        return entity;
    }

    public LivingEntityUpdateEvent setEntity(Entity entity) {
        this.entity = entity;
        return this;
    }

    public LivingEntity getLivingEntity() {
        return livingEntity;
    }

    public LivingEntityUpdateEvent setLivingEntity(LivingEntity livingEntity) {
        this.livingEntity = livingEntity;
        return this;
    }
}
