package com.doubleparallax.metro.event.transformer;

import com.doubleparallax.metro.component.Component;
import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventFactory;
import com.doubleparallax.metro.event.impl.*;
import com.doubleparallax.metro.util.MinecraftHelper;
import net.minecraftforge.client.event.ClientChatEvent;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Component(priority = 9)
public class ForgeEventTransformer implements MinecraftHelper {

    private static final Logger LOGGER = LogManager.getLogger();

    private final EventFactory eventFactory = ComponentFactory.INSTANCE.getComponent(EventFactory.class);

    public ForgeEventTransformer() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void onEvent(ClientChatEvent e) {
        LOGGER.debug("Transforming " + ClientChatEvent.class.getSimpleName() + " to " + ChatEvent.class.getSimpleName());
        ChatEvent event = new ChatEvent().setMessage(e.getMessage());
        LOGGER.debug("Firing " + ChatEvent.class.getSimpleName() + " event");
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable()) {
            LOGGER.debug(ChatEvent.class.getSimpleName() + " event was cancelled, cancelling Forge event");
            e.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onEvent(LivingEvent.LivingUpdateEvent e) {
        LivingEntityUpdateEvent event = new LivingEntityUpdateEvent().setEntity(e.getEntity()).setLivingEntity(e.getEntityLiving());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable())
            e.setCanceled(true);
    }

    @SubscribeEvent
    public void onEvent(GuiOpenEvent e) {
        ScreenEvent.Open event = new ScreenEvent.Open().setOldScreen(mc.currentScreen).setNewScreen(e.getGui());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable()) {
            LOGGER.debug(ScreenEvent.Open.class.getSimpleName() + " event was cancelled, cancelling Forge event");
            e.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onEvent(GuiScreenEvent.DrawScreenEvent.Pre e) {
        ScreenEvent.Render event = new ScreenEvent.Render().setState(Event.State.PRE).setMx(e.getMouseX()).setMy(e.getMouseY()).setRenderPartialTicks(e.getRenderPartialTicks()).setMatrixStack(e.getMatrixStack());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable())
            e.setCanceled(true);
    }

    @SubscribeEvent
    public void onEvent(GuiScreenEvent.MouseClickedEvent e) {
        MouseEvent.Action.Pressed event = new MouseEvent.Action.Pressed().setState((e instanceof GuiScreenEvent.MouseClickedEvent.Pre) ? Event.State.PRE : Event.State.POST).setButton(e.getButton()).setMx(mc.mouseHelper.getMouseX()).setMy(mc.mouseHelper.getMouseY());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable())
            e.setCanceled(true);
    }

    @SubscribeEvent
    public void onEvent(GuiScreenEvent.MouseReleasedEvent e) {
        MouseEvent.Action.Released event = new MouseEvent.Action.Released().setState((e instanceof GuiScreenEvent.MouseReleasedEvent.Pre) ? Event.State.PRE : Event.State.POST).setButton(e.getButton()).setMx(mc.mouseHelper.getMouseX()).setMy(mc.mouseHelper.getMouseY());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable())
            e.setCanceled(true);
    }

    @SubscribeEvent
    public void onEvent(GuiScreenEvent.MouseScrollEvent e) {
        MouseEvent.Scrolled event = new MouseEvent.Scrolled().setState((e instanceof GuiScreenEvent.MouseScrollEvent.Pre) ? Event.State.PRE : Event.State.POST).setScroll(e.getScrollDelta()).setMx(mc.mouseHelper.getMouseX()).setMy(mc.mouseHelper.getMouseY());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable())
            e.setCanceled(true);
    }

    @SubscribeEvent
    public void onEvent(GuiScreenEvent.KeyboardKeyPressedEvent e) {
        KeyboardEvent.Action.Pressed event = new KeyboardEvent.Action.Pressed().setState((e instanceof GuiScreenEvent.KeyboardKeyPressedEvent.Pre) ? Event.State.PRE : Event.State.POST).setKeyCode(e.getKeyCode()).setScanCode(e.getScanCode()).setModifiers(e.getModifiers());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable())
            e.setCanceled(true);
    }

    @SubscribeEvent
    public void onEvent(GuiScreenEvent.KeyboardKeyReleasedEvent e) {
        KeyboardEvent.Action.Released event = new KeyboardEvent.Action.Released().setState((e instanceof GuiScreenEvent.KeyboardKeyReleasedEvent.Pre) ? Event.State.PRE : Event.State.POST).setKeyCode(e.getKeyCode()).setScanCode(e.getScanCode()).setModifiers(e.getModifiers());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable())
            e.setCanceled(true);
    }

    @SubscribeEvent
    public void onEvent(GuiScreenEvent.KeyboardCharTypedEvent e) {
        KeyboardEvent.CharTyped event = new KeyboardEvent.CharTyped().setState((e instanceof GuiScreenEvent.KeyboardCharTypedEvent.Pre) ? Event.State.PRE : Event.State.POST).setCharTyped(e.getCodePoint()).setModifiers(e.getModifiers());
        eventFactory.onEvent(event);
        if (event.isCancelled() && e.isCancelable())
            e.setCanceled(true);
    }

    @SubscribeEvent
    public void onEvent(TickEvent.RenderTickEvent e) {
        if (e.phase == TickEvent.Phase.START)
            eventFactory.onEvent(new ClientUpdateEvent());
    }

}
