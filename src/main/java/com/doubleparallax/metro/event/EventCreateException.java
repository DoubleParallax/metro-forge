package com.doubleparallax.metro.event;

public class EventCreateException extends RuntimeException {

    public EventCreateException(String msg) {
        super(msg);
    }

    public EventCreateException(String msg, Throwable e) {
        super(msg, e);
    }

}
