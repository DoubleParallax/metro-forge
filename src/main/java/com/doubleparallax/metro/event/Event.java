package com.doubleparallax.metro.event;

public abstract class Event<T extends Event<T>> {

    public enum State {
        PRE, POST
    }

    private T self = (T) this;
    private State state;
    private boolean cancelled;

    public State getState() {
        return state;
    }

    public T setState(State state) {
        this.state = state;
        return self;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public T setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
        return self;
    }

}
