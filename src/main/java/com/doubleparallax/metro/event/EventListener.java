package com.doubleparallax.metro.event;

public interface EventListener<T extends Event> {

    void onEvent(T event);

}
