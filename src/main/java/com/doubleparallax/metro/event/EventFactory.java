package com.doubleparallax.metro.event;

import com.doubleparallax.metro.Factory;
import com.doubleparallax.metro.component.Component;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

@Component(priority = 4)
public class EventFactory extends Factory<Class<? extends Event>, Set<EventListener<? extends Event>>> {

    private static final Logger LOGGER = LogManager.getLogger();

    public <T extends Event> void register(Class<T> cls, EventListener<T> listener) {
        LOGGER.debug("Registering event listener " + listener + " for " + cls.getSimpleName() + " events");
        Set<EventListener<?>> listeners = get(cls);
        if (listeners == null) {
            LOGGER.debug("No event listeners found for " + cls.getSimpleName());
            put(cls, listeners = new HashSet<>());
        }
        listeners.add(listener);
    }

    public <T extends Event> void deregister(Class<T> cls, EventListener<T> listener) {
        LOGGER.debug("Registering event listener " + listener + " for " + cls.getSimpleName() + " events");
        Set<EventListener<?>> listeners = get(cls);
        if (listeners != null)
            listeners.remove(listener);
        else LOGGER.warn("No event listeners found for " + cls.getSimpleName());
    }

    public void onEvent(Event event) {
        Set<EventListener<? extends Event>> listeners = get(event.getClass());
        if (listeners == null) return;
        for (EventListener listener : listeners)
            listener.onEvent(event);
    }

}
