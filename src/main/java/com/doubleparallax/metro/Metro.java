package com.doubleparallax.metro;

import com.doubleparallax.metro.component.ComponentFactory;
import com.doubleparallax.metro.util.MinecraftHelper;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod("metro")
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class Metro implements MinecraftHelper {

    private static final Logger LOGGER = LogManager.getLogger();

    public static void onPostInitialize() {
        LOGGER.debug("Initializing Metro components");
        ComponentFactory.INSTANCE.initialize();
    }
}
