package com.doubleparallax.metro.attribute;

import com.doubleparallax.metro.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

public enum AttributeFieldType {
    BOOLEAN(boolean.class, (value) ->
            ("true".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value))
                    ? new Pair<>(null, Boolean.parseBoolean(value))
                    : new Pair<>(String.format("Invalid value '%s', valid input values: true or false", value), null)),
    BYTE(byte.class, (value) -> AttributeFieldType.parseNumber(value, Byte::parseByte, Byte.MIN_VALUE, Byte.MAX_VALUE)),
    CHAR(char.class, (value) ->
            (value.length() > 0)
                    ? new Pair<>(null, value.charAt(0))
                    : new Pair<>("Invalid value, must not be empty", null)),
    SHORT(short.class, (value) -> AttributeFieldType.parseNumber(value, Short::parseShort, Short.MIN_VALUE, Short.MAX_VALUE)),
    INT(int.class, (value) -> AttributeFieldType.parseNumber(value, Integer::parseInt, Integer.MIN_VALUE, Integer.MAX_VALUE)),
    LONG(long.class, (value) -> AttributeFieldType.parseNumber(value, Long::parseLong, Long.MIN_VALUE, Long.MAX_VALUE)),
    FLOAT(float.class, (value) -> AttributeFieldType.parseNumber(value, Float::parseFloat, Float.MIN_VALUE, Float.MAX_VALUE)),
    DOUBLE(double.class, (value) -> AttributeFieldType.parseNumber(value, Double::parseDouble, Double.MIN_VALUE, Double.MAX_VALUE)),
    STRING(String.class, (value) ->
            (!value.isEmpty())
                    ? new Pair<>(null, value)
                    : new Pair<>("Invalid value, must not be empty", null)),
    ENUM(Enum.class, (attribute, value) -> {
        Class<? extends Enum> enumType = attribute.getFieldValue(Enum.class).getClass();
        try {
            return new Pair<>(null, Enum.valueOf(enumType, value.toUpperCase()));
        } catch (IllegalArgumentException e) {
            AttributeFieldType.getLogger().warn("Failed to parse value from " + value, e);
        }
        Set<String> values = new HashSet<>();
        for (Enum<?> val : enumType.getEnumConstants())
            values.add(val.name());
        return new Pair<>(String.format("Invalid value '%s', valid input values: %s", value, String.join(", ", values)), null);
    }),
    OBJECT(AttributeType.class, (attribute, value) -> {
        AttributeType<?> type = attribute.getFieldValue(AttributeType.class);
        Object val = type.parse(value);
        return val == null
                ? new Pair<>(String.format("Invalid value '%s', valid input values: %s", value, type.validInput()), null)
                : new Pair<>(null, val);
    }),
    UNKNOWN(Object.class, (value) -> {
        throw new UnsupportedOperationException();
    });

    private static final Logger LOGGER = LogManager.getLogger();

    private final Class<?> type;
    private Function<String, Pair<String, ?>> function;
    private BiFunction<Attribute, String, Pair<String, ?>> biFunction;

    AttributeFieldType(Class<?> type, Function<String, Pair<String, ?>> function) {
        this.type = type;
        this.function = function;
    }

    AttributeFieldType(Class<?> type, BiFunction<Attribute, String, Pair<String, ?>> biFunction) {
        this.type = type;
        this.biFunction = biFunction;
    }

    public Object parse(Attribute attribute, String value) {
        return function != null ? function.apply(value) : biFunction.apply(attribute, value);
    }

    public static AttributeFieldType getFieldType(Class<?> cls) {
        for (AttributeFieldType type : values())
            if (type.type.getTypeName().equals(cls.getTypeName()))
                return type;
        return AttributeFieldType.UNKNOWN;
    }

    private static Pair<String, ?> parseNumber(String value, Function<String, ?> func, Number min, Number max) {
        try {
            return new Pair<>(null, func.apply(value));
        } catch (NumberFormatException e) {
            AttributeFieldType.LOGGER.warn("Failed to parse value from " + value, e);
        }
        return new Pair<>(String.format("Invalid value '%s', valid input values: %s to %s", value, min, max), null);
    }

    private static Logger getLogger() {
        return LOGGER;
    }

}
