package com.doubleparallax.metro.attribute;

import com.doubleparallax.metro.Factory;
import com.doubleparallax.metro.util.ClassHelper;
import com.doubleparallax.metro.util.Locale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Attribute extends Factory<String, Attribute> {

    private static final Logger LOGGER = LogManager.getLogger();

    private Field field;
    private Attribute root, parent;
    private String parentId;
    private final String id, name, desc;
    private final String[] alias;

    public Attribute() {
        LOGGER.debug("Populating class attribute " + getClass().getSimpleName());
        AttributeDescriptor descriptor = getClass().getAnnotation(AttributeDescriptor.class);
        if (descriptor == null)
            throw new AttributeInitializeException("No descriptor found for root attribute " + getClass().getSimpleName());
        if (!descriptor.parent().isEmpty())
            throw new AttributeInitializeException("Root attribute " + getClass().getSimpleName() + " cannot have a parent");
        id = descriptor.value();
        name = Locale.translate(String.format("attribute.%s.name", getId()));
        desc = Locale.translate(String.format("attribute.%s.desc", getId()));
        alias = Locale.translate(String.format("attribute.%s.alias", getId())).split(",");
        LOGGER.debug(String.format("Populated field attribute, id(%s) name(%s) desc(%s) alias(%s)", getId(), getName(), getDesc(), Arrays.toString(getAlias())));

        LOGGER.debug("Populating field attributes");
        for (Field field : ClassHelper.getFieldsAnnotatedBy(AttributeDescriptor.class, getClass())) {
            LOGGER.debug("Found field " + field.getName() + ", creating attribute");
            Attribute attribute = new Attribute(this, field);
            put(attribute.getId(), attribute);
        }
        for (Attribute attribute : getValues())
            if (attribute.getParentId() != null) {
                Attribute parent = get(attribute.getParentId());
                this.parent = parent;
                parent.put(attribute.id, attribute);
            }
        LOGGER.debug("Found " + getMap().size() + " attributes for " + getId());
    }

    private Attribute(Attribute root, Field field) {
        this.root = root;
        this.field = field;

        LOGGER.debug("Populating field attribute " + field.getName() + " in class " + field.getDeclaringClass().getSimpleName());
        AttributeDescriptor rootDescriptor = field.getDeclaringClass().getAnnotation(AttributeDescriptor.class);
        AttributeDescriptor descriptor = field.getAnnotation(AttributeDescriptor.class);
        if (descriptor == null)
            throw new AttributeInitializeException("No descriptor found for field attribute " + field.getName() + " for class " + field.getDeclaringClass().getSimpleName());
        if (!descriptor.parent().isEmpty()) parentId = descriptor.parent();
        id = descriptor.value();
        name = Locale.translate(String.format("attribute.%s.%s.name", rootDescriptor != null ? rootDescriptor.value() : "_", getId()));
        desc = Locale.translate(String.format("attribute.%s.%s.desc", rootDescriptor != null ? rootDescriptor.value() : "_", getId()));
        alias = Locale.translate(String.format("attribute.%s.%s.alias", rootDescriptor != null ? rootDescriptor.value() : "_", getId())).split(",");
        LOGGER.debug(String.format("Populated field attribute, id(%s) name(%s) desc(%s)", getId(), getName(), getDesc()));
    }

    public String getParentId() {
        return parentId;
    }

    public String getId() {
        return (parentId == null ? "" : parentId + ".") + id;
    }

    public String getName() {
        return name;
    }

    public String getLongName() {
        return (parent == null ? "" : parent.getLongName() + " ") + getName();
    }

    public String getDesc() {
        return desc;
    }

    public String[] getAlias() {
        return alias;
    }

    public boolean setFieldValue(Object value) {
        if (field == null || root == null)
            throw new UnsupportedOperationException("Class attribute does not have a field value to set");
        if (value == null)
            throw new IllegalArgumentException("Cannot set field " + field.getName() + " value to NULL");
        try {
            field.set(root, value);
            root.onFieldValueChange(field, value);
            return true;
        } catch (IllegalAccessException e) {
            LOGGER.warn("Failed to change value for field " + field.getName() + " to " + value);
        }
        return false;
    }

    public Object getFieldValue() {
        return getFieldValue(Object.class);
    }

    public <T> T getFieldValue(Class<T> cls) {
        if (field == null || root == null)
            throw new UnsupportedOperationException("Class attribute does not have a field value to get");
        try {
            return cls.cast(field.get(root));
        } catch (IllegalAccessException e) {
            LOGGER.warn("Failed to get value for field " + field.getName());
        } catch (ClassCastException e) {
            LOGGER.warn("Cannot cast field " + field.getName() + " type " + field.getType().getSimpleName() + " value to " + cls.getSimpleName());
        }
        return null;
    }

    public Object parseStringValue(String value) {
        LOGGER.debug("Parsing value " + value + " for attribute type " + field.getType().getSimpleName());
        return AttributeFieldType.getFieldType(field.getType()).parse(this, value);
    }

    protected void onFieldValueChange(Field field, Object value) {
    }

}
