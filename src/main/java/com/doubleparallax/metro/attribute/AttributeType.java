package com.doubleparallax.metro.attribute;

public interface AttributeType<T> {

    String validInput();

    T parse(String value);

    @Override
    String toString();

}
