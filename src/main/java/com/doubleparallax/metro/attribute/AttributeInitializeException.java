package com.doubleparallax.metro.attribute;

public class AttributeInitializeException extends RuntimeException {

    public AttributeInitializeException(String msg) {
        super(msg);
    }

    public AttributeInitializeException(String msg, Throwable exception) {
        super(msg, exception);
    }

}
