package com.doubleparallax.metro.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class ClassHelper {

    public static Set<Field> getFieldsAnnotatedBy(Class<? extends Annotation> annotation, Class<?> cls) {
        Set<Field> fields = new HashSet<>();
        addFieldsAnnotatedByToCollection(annotation, cls, fields);
        if (cls.getSuperclass() != null)
            addFieldsAnnotatedByToCollection(annotation, cls.getSuperclass(), fields);
        for (Class<?> inter : cls.getInterfaces())
            addFieldsAnnotatedByToCollection(annotation, inter, fields);
        return fields;
    }

    private static void addFieldsAnnotatedByToCollection(Class<? extends Annotation> annotation, Class<?> cls, Set<Field> fields) {
        for (Field field : cls.getFields())
            if (field.isAnnotationPresent(annotation))
                fields.add(field);
    }

}
