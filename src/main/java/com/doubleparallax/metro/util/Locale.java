package com.doubleparallax.metro.util;

import net.minecraft.client.resources.I18n;

public class Locale {

    public static String translate(String key) {
        return I18n.hasKey(key) ? I18n.format(key) : key;
    }

}
