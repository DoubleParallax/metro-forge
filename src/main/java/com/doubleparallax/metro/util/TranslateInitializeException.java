package com.doubleparallax.metro.util;

public class TranslateInitializeException extends RuntimeException {

    public TranslateInitializeException(String msg) {
        super(msg);
    }

    public TranslateInitializeException(String msg, Throwable exception) {
        super(msg, exception);
    }

}
