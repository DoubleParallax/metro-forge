package com.doubleparallax.metro.util;

public class MathHelper {

    public static boolean isInside(double x, double y, double x1, double y1, double w, double h) {
        return isInsideRaw(x, y, x1, y1, x1 + w, y1 + h);
    }

    public static boolean isInsideRaw(double x, double y, double x1, double y1, double x2, double y2) {
        return x >= x1 && x < x2 && y >= y1 && y < y2;
    }

    public static double bound(double x, double min, double max) {
        return Math.min(Math.max(min, x), max);
    }

}
