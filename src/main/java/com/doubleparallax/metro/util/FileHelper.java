package com.doubleparallax.metro.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class FileHelper {

    private static final Logger LOGGER = LogManager.getLogger();

    public static final String DIRECTORY_INTERNAL_FONT_TEXTURE = "/metro/texture/font/";
    public static final String DIRECTORY_INTERNAL_URL_TEXTURE = "/metro/texture/url/";

    public static String join(String... path) {
        StringBuilder joint = new StringBuilder();
        for (int i = 0; i < path.length; i++) {
            String seg = path[i];
            joint.append(seg);
            if (i < path.length - 1)
                joint.append("/");
        }
        return joint.toString();
    }

    public static InputStream readResourceStream(String path) {
        return FileHelper.class.getResourceAsStream(path);
    }

    public static String readResource(String path) throws IOException {
        InputStream stream = FileHelper.class.getResourceAsStream(path);
        if (stream == null) throw new FileNotFoundException("Failed to find resource " + path);
        Reader reader = new InputStreamReader(stream);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder lines = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null)
            lines.append(line).append("\n");
        return lines.toString();
    }

    public static Set<String> getFilesInDirectory(String path) throws IOException {
        InputStream stream = FileHelper.class.getResourceAsStream(path);
        if (stream == null) throw new FileNotFoundException("Failed to find directory " + path);
        Reader reader = new InputStreamReader(stream);
        BufferedReader bufferedReader = new BufferedReader(reader);
        Set<String> fileNames = new HashSet<>();
        String line;
        while ((line = bufferedReader.readLine()) != null)
            fileNames.add(line);
        return fileNames;
    }

}
