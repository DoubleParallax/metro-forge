package com.doubleparallax.metro.util;

import net.minecraft.client.Minecraft;
import net.minecraft.util.Util;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.text.StringTextComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ChatHelper {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Minecraft mc = Minecraft.getInstance();

    public static void sendChatMessage(String message, Object... args) {
        if (mc.ingameGUI == null) {
            LOGGER.debug("Cannot send chat messages before a world has loaded");
            return;
        }
        if (args.length > 0) message = String.format(message.replaceAll("%s", "\247e%s\2477"), args);
        message = "\2477" + message;
        LOGGER.info("Sending client only chat message: " + message);
        mc.ingameGUI.func_238450_a_(ChatType.SYSTEM, new StringTextComponent(message), Util.field_240973_b_);
    }

}
