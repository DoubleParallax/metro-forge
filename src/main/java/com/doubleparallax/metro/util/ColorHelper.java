package com.doubleparallax.metro.util;

public class ColorHelper {

    public static float[] intToFloat(int color) {
        return new float[]{
                (color >> 16 & 255) / 255f,
                (color >> 8 & 255) / 255f,
                (color & 255) / 255f,
                (color >> 24 & 255) / 255f
        };
    }

    public static int[] intToInt(int color) {
        return new int[]{
                color >> 16 & 255,
                color >> 8 & 255,
                color & 255,
                color >> 24 & 255
        };
    }

}
