package com.doubleparallax.metro.util.render.font;

import com.doubleparallax.metro.Factory;
import com.doubleparallax.metro.component.Component;
import com.doubleparallax.metro.util.FileHelper;
import com.doubleparallax.metro.util.MinecraftHelper;
import com.doubleparallax.metro.util.render.font.impl.FontProfile;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

@Component(priority = 1)
public class FontFactory extends Factory<String, FontProfile> implements MinecraftHelper {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Gson GSON = new Gson();

    private static final String FONT_DIRECTORY = "assets/metro/font/";
    private static final Pattern FONT_SCAN_PATTERN = Pattern.compile("^.*\\.json$");

    public FontFactory() {
        LOGGER.debug("Populating font factory map with font definition resource files");
        new Reflections("", new ResourcesScanner()).getResources(FONT_SCAN_PATTERN)
                .forEach((resource) -> {
                    if (!resource.startsWith(FONT_DIRECTORY)) {
                        LOGGER.debug("Found JSON file, but not a font definition, ignoring " + resource);
                        return;
                    }
                    LOGGER.debug("Initializing font " + resource);
                    try {
                        String json = FileHelper.readResource("/" + resource);
                        FontDefinition definition = GSON.fromJson(json, FontDefinition.class);
                        if (definition == null)
                            throw new JsonParseException("Font definition file is not of the correct format");

                        for (float size : definition.getSizes()) {
                            try {
                                InputStream inputStream = FileHelper.readResourceStream(String.format("/assets/metro/font/%s.ttf", definition.getName()));
                                Font font = Font.createFont(Font.TRUETYPE_FONT, inputStream).deriveFont(size);
                                put(String.format("%s:%s", definition.getName(), size), new FontProfile(font, true, true));
                                LOGGER.debug("Initialized font " + resource);
                            } catch (FontFormatException | IOException e) {
                                LOGGER.warn("Failed to initialize font for asset " + resource);
                                throw e;
                            }
                        }

                        LOGGER.debug("Initialized font " + resource);
                    } catch (IOException | JsonParseException | FontFormatException e) {
                        LOGGER.error("Failed to read or parse font definition file " + resource, e);
                    }
                });
    }

}
