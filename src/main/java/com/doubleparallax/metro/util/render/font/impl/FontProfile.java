package com.doubleparallax.metro.util.render.font.impl;

import com.doubleparallax.metro.util.FileHelper;
import com.doubleparallax.metro.util.MinecraftHelper;
import com.doubleparallax.metro.util.render.font.FontInitializeException;
import com.doubleparallax.metro.util.render.geometry.Rect2D;
import com.doubleparallax.metro.util.render.texture.TextureHelper;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.NativeImage;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FontProfile implements MinecraftHelper {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final int TEXTURE_WIDTH = 512;
    private static final int TEXTURE_HEIGHT = 512;

    private static final int[] COLORS = new int[32];
    private static final String COLOR_IDENTIFIERS = "0123456789abcdefklmnor";

    private static final Random RANDOM = new Random();

    static {
        final int multiplier = 170;
        final int divider = 4;
        for (int i = 0; i < COLORS.length; i++) {
            int bias = (i >> 3 & 1) * 85;
            int red = (i >> 2 & 1) * multiplier + bias;
            int green = (i >> 1 & 1) * multiplier + bias;
            int blue = (i & 1) * multiplier + bias;
            if (i == 6) red += 85;
            if (i >= 16) {
                red /= divider;
                green /= divider;
                blue /= divider;
            }
            COLORS[i] = (red & 255) << 16
                    | (green & 255) << 8
                    | (blue & 255)
                    | (i >= 16 ? 128 : 255) << 24;
        }
    }

    private final Glyph[] glyphs = new Glyph[256],
            glyphsBold = new Glyph[256],
            glyphsItalic = new Glyph[256],
            glyphsBoldItalic = new Glyph[256];
    private final ResourceLocation texture, textureBold, textureItalic, textureBoldItalic;
    private final Map<RenderingHints.Key, Object> renderingHints = new HashMap<>();

    private int fontHeight = -1;

    public FontProfile(Font font, boolean antialias, boolean fractionalMetrics) {
        renderingHints.put(RenderingHints.KEY_FRACTIONALMETRICS, fractionalMetrics ? RenderingHints.VALUE_FRACTIONALMETRICS_ON : RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
        renderingHints.put(RenderingHints.KEY_TEXT_ANTIALIASING, antialias ? RenderingHints.VALUE_TEXT_ANTIALIAS_ON : RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        renderingHints.put(RenderingHints.KEY_ANTIALIASING, antialias ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);

        texture = generateTexture(font.deriveFont(Font.PLAIN), glyphs);
        if (texture == null)
            throw new FontInitializeException("Failed to generate image texture for font " + font.getName());

        textureBold = generateTexture(font.deriveFont(Font.BOLD), glyphsBold);
        if (textureBold == null)
            throw new FontInitializeException("Failed to generate image texture for bold font " + font.getName());

        textureItalic = generateTexture(font.deriveFont(Font.ITALIC), glyphsItalic);
        if (textureItalic == null)
            throw new FontInitializeException("Failed to generate image texture for italic font " + font.getName());

        textureBoldItalic = generateTexture(font.deriveFont(Font.BOLD | Font.ITALIC), glyphsBoldItalic);
        if (textureBoldItalic == null)
            throw new FontInitializeException("Failed to generate image texture for bold italic font " + font.getName());
    }

    private ResourceLocation generateTexture(Font font, Glyph[] glyphs) {
        LOGGER.debug("Generating texture for font profile " + font.getName());
        BufferedImage bufferedImage = generateFontImage(font, glyphs);
        DynamicTexture texture = TextureHelper.getNativeImage(bufferedImage, TEXTURE_WIDTH, TEXTURE_HEIGHT);
        if (texture == null) {
            LOGGER.warn(String.format("Failed to get %s from buffered image for font %s", NativeImage.class, font.getName()));
            return null;
        }
        return mc.getTextureManager().getDynamicTextureLocation(String.format("%s/%s-%s-%s", FileHelper.DIRECTORY_INTERNAL_FONT_TEXTURE, font.getName().toLowerCase(), font.getSize(), font.getStyle()), texture);
    }

    private BufferedImage generateFontImage(Font font, Glyph[] glyphs) {
        LOGGER.debug("Generating font image for font " + font.getName());
        BufferedImage bufferedImage = new BufferedImage(TEXTURE_WIDTH, TEXTURE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = (Graphics2D) bufferedImage.getGraphics();

        LOGGER.debug("Preconfiguring base image");
        graphics2D.setFont(font);
        graphics2D.setColor(new Color(255, 255, 255, 0));
        graphics2D.fillRect(0, 0, TEXTURE_WIDTH, TEXTURE_HEIGHT);
        graphics2D.setColor(Color.WHITE);
        graphics2D.setRenderingHints(renderingHints);

        FontMetrics fontMetrics = graphics2D.getFontMetrics();

        int charHeight = 0;
        int x = 0;
        int y = 0;

        LOGGER.debug("Rendering individual characters to image map");
        for (int i = 0; i < glyphs.length; i++) {
            char character = (char) i;
            String characterString = String.valueOf(character);
            Rectangle2D bounds = fontMetrics.getStringBounds(characterString, graphics2D);
            int w = bounds.getBounds().width + 8;
            int h = bounds.getBounds().height;

            if (x + w >= TEXTURE_WIDTH) {
                x = 0;
                y += charHeight;
                charHeight = 0;
            }

            if (h > charHeight) charHeight = h;
            if (h > fontHeight) fontHeight = h;

            graphics2D.drawString(characterString, x + 2, y + fontMetrics.getAscent());

            glyphs[i] = new Glyph(x, y, w, h, TEXTURE_WIDTH, TEXTURE_HEIGHT);
            x += w;
        }
        return bufferedImage;
    }

    private void setupRender(MatrixStack matrixStack) {
        matrixStack.push();

        RenderSystem.enableBlend();
        RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        RenderSystem.color4f(1, 1, 1, 0);
        RenderSystem.enableTexture();
        mc.getTextureManager().bindTexture(texture);
    }

    private void shutdownRender(MatrixStack matrixStack) {
        GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_DONT_CARE);
        RenderSystem.bindTexture(0);
        matrixStack.pop();
    }

    public void drawString(MatrixStack matrixStack, String text, double x, double y, int color) {
        x = Math.round(x);
        y = Math.round(y + 1);

        if (text == null) return;

        float alpha = (color >> 24 & 255) / 255f;
        if (alpha < 0.01f)
            return;

        boolean obfuscated, bold, strike, underline, italic;
        obfuscated = bold = strike = underline = italic = false;

        setupRender(matrixStack);

        Glyph[] glyphs = this.glyphs;
        int renderColor = color;
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char character = text.charAt(i);
            if (character == '\247' && i < length - 1) {
                int colorIdentifier = COLOR_IDENTIFIERS.indexOf(text.charAt(i++ + 1));
                switch (colorIdentifier) {
                    case -1:
                        break;
                    case 16:
                        obfuscated = true;
                        break;
                    case 17:
                        bold = true;
                        mc.getTextureManager().bindTexture(italic ? textureBoldItalic : textureBold);
                        glyphs = italic ? glyphsBoldItalic : glyphsBold;
                        break;
                    case 18:
                        strike = true;
                        break;
                    case 19:
                        underline = true;
                        break;
                    case 20:
                        italic = true;
                        mc.getTextureManager().bindTexture(bold ? textureBoldItalic : textureItalic);
                        glyphs = bold ? glyphsBoldItalic : glyphsItalic;
                        break;
                    default:
                        obfuscated = bold = strike = underline = italic = false;
                        renderColor = COLORS[colorIdentifier];
                        mc.getTextureManager().bindTexture(texture);
                        glyphs = this.glyphs;
                        break;
                }
                if (colorIdentifier == 21)
                    renderColor = color;
                continue;
            } else if (character > glyphs.length) continue;

            Glyph glyph = glyphs[character];
            Glyph originalGlyph = glyph;
            if (obfuscated) glyph = glyphs[RANDOM.nextInt(glyphs.length)];

            float w = glyph.getW();
            float h = glyph.getH();
            float tx = glyph.getTextureX();
            float ty = glyph.getTextureY();
            float tw = glyph.getTextureW();
            float th = glyph.getTextureH();

            Rect2D.Data glyphData = new Rect2D.Data().setPos(x, y, x + w, y + h).setTex(tx, ty, tx + tw, ty + th).setColor(renderColor).setAlpha(alpha);
            Rect2D.draw(matrixStack, glyphData);

            float hh = (h / 2f) - 1;

            Rect2D.Data data = new Rect2D.Data().setPos(x, y + hh, x + w, y + hh + 2).setColor(renderColor).setAlpha(alpha);
            if (strike) Rect2D.draw(matrixStack, data);
            if (underline) Rect2D.draw(matrixStack, data.setPos(x, y + h, x + w, y + h + 2));

            x += originalGlyph.getW() - 8;
        }

        shutdownRender(matrixStack);
    }

    public double getWidth(String text) {
        if (text == null) return 0;

        boolean bold, italic;
        bold = italic = false;

        double w = 0;

        Glyph[] glyphs = this.glyphs;
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char character = text.charAt(i);
            if (character == '\247' && i < length - 1) {
                int colorIdentifier = COLOR_IDENTIFIERS.indexOf(text.charAt(i++ + 1));
                switch (colorIdentifier) {
                    case 17:
                        bold = true;
                        glyphs = italic ? glyphsBoldItalic : glyphsBold;
                        break;
                    case -1:
                    case 16:
                    case 18:
                    case 19:
                        break;
                    case 20:
                        italic = true;
                        glyphs = bold ? glyphsBoldItalic : glyphsItalic;
                        break;
                    default:
                        bold = italic = false;
                        glyphs = this.glyphs;
                        break;
                }
                continue;
            } else if (character > glyphs.length) continue;

            Glyph glyph = glyphs[character];
            w += glyph.getW() - 8;
        }
        return w;
    }

    public double getHeight(String text) {
        if (text == null) return 0;

        boolean bold, italic;
        bold = italic = false;

        double h = 0;

        Glyph[] glyphs = this.glyphs;
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char character = text.charAt(i);
            if (character == '\247' && i < length - 1) {
                int colorIdentifier = COLOR_IDENTIFIERS.indexOf(text.charAt(i++ + 1));
                switch (colorIdentifier) {
                    case 17:
                        bold = true;
                        glyphs = italic ? glyphsBoldItalic : glyphsBold;
                        break;
                    case -1:
                    case 16:
                    case 18:
                    case 19:
                        break;
                    case 20:
                        italic = true;
                        glyphs = bold ? glyphsBoldItalic : glyphsItalic;
                        break;
                    default:
                        bold = italic = false;
                        glyphs = this.glyphs;
                        break;
                }
                continue;
            } else if (character > glyphs.length) continue;

            Glyph glyph = glyphs[character];
            h = Math.max(h, glyph.getH());
        }
        return h;
    }

}
