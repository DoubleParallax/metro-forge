package com.doubleparallax.metro.util.render.texture;

import com.doubleparallax.metro.util.ColorHelper;
import com.doubleparallax.metro.util.FileHelper;
import com.doubleparallax.metro.util.MinecraftHelper;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.NativeImage;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TextureHelper implements MinecraftHelper {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Map<String, Texture> CACHE = new HashMap<>();

    private static final ExecutorService DOWNLOAD_THREAD_POOL = Executors.newFixedThreadPool(8);

    public static DynamicTexture getNativeImage(BufferedImage bufferedImage, int width, int height) {
        if (width * height == 0) {
            LOGGER.warn("Image width and height must not be zero");
            return null;
        }

        LOGGER.debug("Converting image data to " + NativeImage.class.getSimpleName());
        NativeImage nativeImage = new NativeImage(NativeImage.PixelFormat.RGBA, width, height, true);
        for (int i = 0; i < width * height; i++) {
            int x = i % width;
            int y = Math.floorDiv(i, width);
            nativeImage.setPixelRGBA(x, y, bufferedImage.getRGB(x, y));
        }
        return new DynamicTexture(nativeImage);
    }

    public static Texture download(String url) {
        Texture texture = CACHE.get(url);
        if (texture == null) {
            LOGGER.debug("Texture not cached, fetching new asset");
            texture = new Texture();
            CACHE.put(url, texture);
            process(texture, url);
        } else {
            if (texture.isProcessing()) {
                LOGGER.debug("Processing texture resource from raw image");
                DynamicTexture dynamicTexture = getNativeImage(texture.getImage(), texture.getWidth(), texture.getHeight());
                if (dynamicTexture == null) {
                    LOGGER.warn("Could not create dynamic texture for url " + url);
                    return null;
                }
                texture.setResource(mc.getTextureManager().getDynamicTextureLocation(String.format("%s/%s", FileHelper.DIRECTORY_INTERNAL_URL_TEXTURE, url.hashCode()), dynamicTexture));
                return null;
            }
        }
        return texture;
    }

    private static void process(Texture texture, String url) {
        DOWNLOAD_THREAD_POOL.submit(() -> {
            HttpResponse<byte[]> response = Unirest.get(url).asBytes();
            if (response == null) {
                LOGGER.warn(String.format("Failed to download image from URL %s, got no response", url));
                return;
            }
            if (!response.isSuccess()) {
                LOGGER.warn(String.format("Failed to download image from URL %s, status (%s), reason: %s", url, response.getStatus(), response.getStatusText()));
                return;
            }
            try {
                texture.setImage(ImageIO.read(new ByteArrayInputStream(response.getBody())));
            } catch (IOException e) {
                LOGGER.warn("Failed to create image from byte array", e);
            }
        });
    }

}
