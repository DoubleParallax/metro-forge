package com.doubleparallax.metro.util.render.font.impl;

public class Glyph {

    private final int x, y, w, h;
    private final float textureX, textureY, textureW, textureH;

    public Glyph(int x, int y, int w, int h, int textureW, int textureH) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        textureX = (float) x / textureW;
        textureY = (float) y / textureH;
        this.textureW = (float) w / textureW;
        this.textureH = (float) h / textureH;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public float getTextureX() {
        return textureX;
    }

    public float getTextureY() {
        return textureY;
    }

    public float getTextureW() {
        return textureW;
    }

    public float getTextureH() {
        return textureH;
    }
}
