package com.doubleparallax.metro.util.render.texture;

import net.minecraft.util.ResourceLocation;

import java.awt.image.BufferedImage;

public class Texture {

    private ResourceLocation resource;
    private BufferedImage image;

    public Texture() {}

    public Texture(BufferedImage image) {
        this.image = image;
    }

    public boolean isProcessing() {
        return image != null && resource == null;
    }

    public ResourceLocation getResource() {
        return resource;
    }

    public Texture setResource(ResourceLocation resource) {
        this.resource = resource;
        return this;
    }

    public BufferedImage getImage() {
        return image;
    }

    public Texture setImage(BufferedImage image) {
        this.image = image;
        return this;
    }

    public int getWidth() {
        return image == null ? 0 : image.getWidth();
    }

    public int getHeight() {
        return image == null ? 0 : image.getHeight();
    }
}
