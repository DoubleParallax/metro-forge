package com.doubleparallax.metro.util.render.font;

public class FontInitializeException extends RuntimeException {

    public FontInitializeException(String msg) {
        super(msg);
    }

    public FontInitializeException(String msg, Throwable exception) {
        super(msg, exception);
    }

}
