package com.doubleparallax.metro.util.render.geometry;

import com.doubleparallax.metro.util.ColorHelper;
import com.doubleparallax.metro.util.MinecraftHelper;
import com.doubleparallax.metro.util.render.texture.Texture;
import com.doubleparallax.metro.util.render.texture.TextureHelper;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldVertexBufferUploader;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import org.lwjgl.opengl.GL11;

public class Circle2D implements MinecraftHelper {

    public static class Data {
        private ResourceLocation resource;
        private String url;
        private double x1, y1, x2, y2, start = 0, end = 360, step;
        private float tx1 = 0, ty1 = 0, tx2 = 0, ty2 = 0;
        private int c = -1;
        private int mode = GL11.GL_POLYGON;
        private float alpha = 1;

        public Data() {
            setColor(-1);
        }

        public Data setResource(ResourceLocation resource) {
            this.resource = resource;
            return this;
        }

        public Data setUrl(String url) {
            this.url = url;
            return this;
        }

        public Data setPos(double x1, double y1, double x2, double y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            return this;
        }

        public Data setTex(float tx1, float ty1, float tx2, float ty2) {
            this.tx1 = tx1;
            this.ty1 = ty1;
            this.tx2 = tx2;
            this.ty2 = ty2;
            return this;
        }

        public Data setSegment(double start, double end) {
            this.start = start;
            this.end = end;
            return this;
        }

        public Data setStep(double step) {
            this.step = step;
            return this;
        }

        public Data setColor(int c) {
            this.c = c;
            return this;
        }

        public Data setMode(int mode) {
            this.mode = mode;
            return this;
        }

        public Data setAlpha(float alpha) {
            this.alpha = alpha;
            return this;
        }
    }

    public static void draw(MatrixStack matrixStack, Data data) {
        float[] color = ColorHelper.intToFloat(data.c);

        if (data.url != null) {
            Texture texture = TextureHelper.download(data.url);
            if (texture == null || texture.getResource() == null)
                return;
            data.resource = texture.getResource();
        }

        boolean noTextureCoords = (data.tx1 == 0 && data.tx2 == 0 && data.ty1 == 0 && data.ty2 == 0);

        if (data.resource != null) {
            mc.getTextureManager().bindTexture(data.resource);
            if (noTextureCoords) {
                data.tx2 = data.ty2 = 1;
                noTextureCoords = false;
            }
        }

        if (noTextureCoords) RenderSystem.disableTexture();
        else RenderSystem.enableTexture();

        RenderSystem.enableBlend();
        RenderSystem.defaultBlendFunc();

        double w = data.x2 - data.x1;
        double h = data.y2 - data.y1;
        double ww = w / 2.;
        double hh = h / 2.;
        double cx = data.x1 + ww;
        double cy = data.y1 + hh;
        double tw = (data.tx2 - data.tx1) / w;
        double th = (data.ty2 - data.ty1) / h;
        if (data.step == 0) data.step = Math.max(360. / (Math.max(w, h) / 2.), 1);

        Matrix4f matrix = matrixStack.getLast().getMatrix();

        BufferBuilder bufferbuilder = Tessellator.getInstance().getBuffer();
        bufferbuilder.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_COLOR_TEX);

        if (data.start != data.end % 360 && data.mode != GL11.GL_LINE_STRIP)
            bufferbuilder.pos(matrix, (float) cx, (float) cy, 0).color(color[0], color[1], color[2], color[3] * data.alpha).tex(0, 0).endVertex();

        for (double i = Math.max(data.start, data.end); i >= Math.min(data.start, data.end); i -= data.step) {
            double j = Math.toRadians(i);
            double x = cx + (Math.cos(j) * ww);
            double y = cy + (Math.sin(j) * hh);
            double tx = data.tx1 + (tw * (x - data.x1));
            double ty = data.ty1 + (th * (y - data.y1));

            bufferbuilder.pos(matrix, (float) x, (float) (y), 0).color(color[0], color[1], color[2], color[3] * data.alpha).tex((float) tx, (float) ty).endVertex();
        }

        bufferbuilder.finishDrawing();
        WorldVertexBufferUploader.draw(bufferbuilder);

        RenderSystem.enableTexture();
        RenderSystem.disableBlend();
    }

}
