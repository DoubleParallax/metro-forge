package com.doubleparallax.metro.util.render.geometry;

import com.doubleparallax.metro.util.ColorHelper;
import com.doubleparallax.metro.util.MinecraftHelper;
import com.doubleparallax.metro.util.render.texture.Texture;
import com.doubleparallax.metro.util.render.texture.TextureHelper;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldVertexBufferUploader;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Matrix4f;
import org.lwjgl.opengl.GL11;

public class Rect2D implements MinecraftHelper {

    public static class Data {
        private ResourceLocation resource;
        private String url;
        private double x1, y1, x2, y2, x3, y3, x4, y4;
        private float tx1 = 0, ty1 = 0, tx2 = 0, ty2 = 0, tx3 = 0, ty3 = 0, tx4 = 0, ty4 = 0;
        private int c1 = -1, c2 = -1, c3 = -1, c4 = -1;
        private int mode = GL11.GL_QUADS;
        private float alpha = 1;

        public Data() {
            setColor(-1);
        }

        public Data setResource(ResourceLocation resource) {
            this.resource = resource;
            return this;
        }

        public Data setUrl(String url) {
            this.url = url;
            return this;
        }

        public Data incrementPos(double x1, double y1, double x2, double y2) {
            return incrementPos(x1, y1, x2, y1, x2, y2, x1, y2);
        }

        public Data incrementPos(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
            this.x1 += x1;
            this.y1 += y1;
            this.x2 += x2;
            this.y2 += y2;
            this.x3 += x3;
            this.y3 += y3;
            this.x4 += x4;
            this.y4 += y4;
            return this;
        }

        public Data setPos(double x1, double y1, double x2, double y2) {
            return setPos(x1, y1, x2, y1, x2, y2, x1, y2);
        }

        public Data setPos(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.x3 = x3;
            this.y3 = y3;
            this.x4 = x4;
            this.y4 = y4;
            return this;
        }

        public Data setTex(float tx1, float ty1, float tx2, float ty2) {
            return setTex(tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2);
        }

        public Data setTex(float tx1, float ty1, float tx2, float ty2, float tx3, float ty3, float tx4, float ty4) {
            this.tx1 = tx1;
            this.ty1 = ty1;
            this.tx2 = tx2;
            this.ty2 = ty2;
            this.tx3 = tx3;
            this.ty3 = ty3;
            this.tx4 = tx4;
            this.ty4 = ty4;
            return this;
        }

        public Data setColor(int c) {
            return setColor(c, c, c, c);
        }

        public Data setColor(int c1, int c2, int c3, int c4) {
            this.c1 = c1;
            this.c2 = c2;
            this.c3 = c3;
            this.c4 = c4;
            return this;
        }

        public Data setMode(int mode) {
            this.mode = mode;
            return this;
        }

        public Data setAlpha(float alpha) {
            this.alpha = alpha;
            return this;
        }
    }

    public static void draw(MatrixStack matrixStack, Data data) {
        float[][] color = new float[][]{
                ColorHelper.intToFloat(data.c1),
                ColorHelper.intToFloat(data.c2),
                ColorHelper.intToFloat(data.c3),
                ColorHelper.intToFloat(data.c4)
        };

        if (data.url != null) {
            Texture texture = TextureHelper.download(data.url);
            if (texture == null || texture.getResource() == null)
                return;
            data.resource = texture.getResource();
        }

        boolean noTextureCoords = (data.tx1 == 0 && data.tx2 == 0 && data.tx3 == 0 && data.tx4 == 0 && data.ty1 == 0 && data.ty2 == 0 && data.ty3 == 0 && data.ty4 == 0);

        if (data.resource != null) {
            mc.getTextureManager().bindTexture(data.resource);
            if (noTextureCoords) {
                data.tx2 = data.tx3 = data.ty3 = data.ty4 = 1;
                noTextureCoords = false;
            }
        }

        if (noTextureCoords) RenderSystem.disableTexture();
        else RenderSystem.enableTexture();

        RenderSystem.enableBlend();
        RenderSystem.defaultBlendFunc();

        Matrix4f matrix = matrixStack.getLast().getMatrix();

        BufferBuilder bufferbuilder = Tessellator.getInstance().getBuffer();
        bufferbuilder.begin(data.mode, DefaultVertexFormats.POSITION_COLOR_TEX);
        bufferbuilder.pos(matrix, (float) data.x4, (float) data.y4, 0).color(color[3][0], color[3][1], color[3][2], color[3][3] * data.alpha).tex(data.tx4, data.ty4).endVertex();
        bufferbuilder.pos(matrix, (float) data.x3, (float) data.y3, 0).color(color[2][0], color[2][1], color[2][2], color[2][3] * data.alpha).tex(data.tx3, data.ty3).endVertex();
        bufferbuilder.pos(matrix, (float) data.x2, (float) data.y2, 0).color(color[1][0], color[1][1], color[1][2], color[1][3] * data.alpha).tex(data.tx2, data.ty2).endVertex();
        bufferbuilder.pos(matrix, (float) data.x1, (float) data.y1, 0).color(color[0][0], color[0][1], color[0][2], color[0][3] * data.alpha).tex(data.tx1, data.ty1).endVertex();
        bufferbuilder.finishDrawing();
        WorldVertexBufferUploader.draw(bufferbuilder);

        RenderSystem.enableTexture();
        RenderSystem.disableBlend();
    }

}
