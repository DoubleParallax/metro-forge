package com.doubleparallax.metro.util.render.font;

public class FontDefinition {

    private String name;
    private float[] sizes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float[] getSizes() {
        return sizes;
    }

    public void setSizes(float[] sizes) {
        this.sizes = sizes;
    }
}
