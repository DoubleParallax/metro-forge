# Metro Forge

[![forthebadge](https://forthebadge.com/images/badges/made-with-crayons.svg)](https://forthebadge.com)


### Project Objectives
The main aim of this project is to create a baseline Minecraft Forge mod standard others can either learn from or use to begin their own journey through making Forge compatible mods for Minecraft.
Due to the nature of Forge mods, unit and integration testing will not be covered in this version.

### Current Base Mod Functionality
##### Core Transformers
To prevent conflicts with other Forge compatible mods, core transformers should be kept to a minimum, and only used when absolutely necessary. Too many may cause restrictions with compatibility.

##### Mod Components
Slightly inspired by Spring, this custom component factory allows auto-initializing of custom components throughout the mod, e.g. CommandFactory, PluginFactory, FontFactory, etc. without having to reference a static singleton instance of the class.
The component factory will process all classes annotated with `@Component` with their priority ordered accordingly (0 = highest, 10+ = lowest).

##### Command Factory Component
The command factory is where all the command initialization processes are handled, automatically scanning for sub types of `Command` to initialize, ensuring the `Command` sub type is annotated with `@CommandDescriptor` to ensure the command is registered correctly. The value provided in the command descriptor annotation is to provide the "root name" of the command, for all locale purposes, and the entries provided in the locale are to follow the format:
|Entry|Key|Example|
|-|-|-|
|Name|`command.<root>.name`|`command.example.name=Example Command`|
|Description|`command.<root>.desc`|`command.example.desc=An example command`|
|Alias|`command.<root>.alias` (comma separated)|`command.example.alias=example,exmpl,ex`|

##### Event Factory Component
The event factory is the core of all Metro specific events, and event transformers from other Mod Loader types. Currently the only implemented Mod Loader event transformer is Forge, but this may change depending on the demand for others like LiteLoader, Fabric, etc.
Events are transformed to conform with a single standard throughout Metro, making it easier to switch between Mod Loaders

##### Plugin Factory Component
The plugin factory is responsible for loading all configurable enhancements of Metro, from Aimbot, through Fly, to Xray. The plugin factory will scan for all sub types of `Plugin` and initialize accordingly. Plugins can additionally have configurable properties as direct class field members, annotated with `@AttributeDescriptor`. If a plugin class extends `Attribute`, then it will automatically process these fields on initialization. The value provided in the attribute descriptor for the plugin class is to provide the "root name" of the plugin, for all locale purposes, and the entries provided in the locale are to follow the format:
|Entry|Key|Example|
|-|-|-|
|Name|`attribute.<root>.name`|`attribute.example.name=Example Plugin`|
|Description|`attribute.<root>.desc`|`attribute.example.desc=An example plugin`|
|Alias|`attribute.<root>.alias` (comma separated)|`attribute.example.alias=example,exmpl,ex`|

##### Attributes
If a plugin has marked a field as an attribute with the `@AttributeDescriptor` annotation, then this field will be treated accordingly in the respective components (Commands, and UI). The values of these fields will also be stored along with the rest of the mod configuration. If an attribute is not a primitive type or an `Enum`, the type will need to implement `AttributeType` to ensure it can be parsed and retrieved in the correct format. If an attribute descriptor has a non-empty "parent" value, the attribute will be linked to the corresponding "parent", matching the attribute descriptor value.
Example `Attribute` class:
```
@AttributeDescriptor("example")
public class Example extends Attribute {
    @AttributeDescriptor("background")
    public boolean background = false;
    
        @AttributeDescriptor(parent = "background", value = "red")
        public int backgroundRed = 128;
    
            @AttributeDescriptor(parent = "background.red", value = "min")
            public int backgroundRedMin = 0;
}
```
The value field of the attribute descriptor for class field members must match the following format:
|Entry|Key|Example|
|-|-|-|
|Name|`attribute.<class_root>.[<parent>].<attribute>.name`|`attribute.example.background.red.name=Red`|
|Description|`attribute.<class_root>.[<parent>].<attribute>.desc`|`attribute.example.background.red.desc=The red color aspect`|
|Alias|`attribute.<root>.[<parent>].<attribute>.alias` (comma separated)|`attribute.example.background.red.alias=red,r`|

##### UI Factory Component
A large part of Metro is going to be the custom UI container. The UI factory will process sub types of `UI` and ensure they're annotated correctly with `@UIDescriptor`. This will ensure when a standard Minecraft `Screen` is opened, if there is a corresponding UI linked, the new UI will be displayed and interacted with instead, assuming the Plugin `Menu Overhaul` is active at the time, or the UI descriptor signified a manual override of the active check.

##### Font Factory Component
The current font factory makes use of the pre-existing font rendering system in Minecraft. Font's are provided as a JSON and respective TTF file in the `/assets/metro/font` resource directory. The file names don't have to match, but to keep things consistent, it helps to. The contents of the `<font>.json` file should match the following format:
```
{
    "name": "myFont",
    "sizes": [16, 24, 32, 48]
}
```
The `name` attribute is where the file must match the name of, i.e. for the above example, a file called `myFont.ttf` must exist in the aforementioned resource directory.
Post initialization, the font renderer that would be created can be accessed by the `FontFactory` component, using `fontFactory.get("myFont:<size>");` where `size` is one of the predefined sizes in the related JSON descriptor.

##### Locale
Due to not wanting to be restricted to only using Forge for the versions of this mod, I have implemented custom internationalization. Currently the only language supported is English, as that's my native language, but will be opening it to the public to provide translations in other languages. To add new translations, just add a `key=value` formatted entry into the `/assets/metro/lang/en.lang` resource, and then when you want to retrieve the value, using `Locale.translate("my.key")` will suffice. The language will be configurable at a later date TBD.